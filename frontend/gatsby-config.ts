import type { GatsbyConfig } from "gatsby";

const plugins = ["gatsby-plugin-material-ui", "gatsby-theme-material-ui"];

if (process.env.NODE_ENV !== "development")
  plugins.push("gatsby-plugin-eslint");

const config: GatsbyConfig = {
  siteMetadata: {
    title: `semestral-frontend`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins,
};

export default config;
