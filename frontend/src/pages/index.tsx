import * as React from "react";
import type { HeadFC, PageProps } from "gatsby";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Stack,
  Typography,
} from "@mui/material";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import PersonIcon from "@mui/icons-material/Person";
import BookIcon from "@mui/icons-material/Book";
import { useEffect, useState } from "react";

const IndexPage: React.FC<PageProps> = () => {
  const [bookCount, setBookCount] = useState(0);
  const [userCount, setUserCount] = useState(0);
  const [rentCount, setRentCount] = useState(0);

  useEffect(() => {
    getBooks();
    getUsers();
    getRents();
  }, []);

  const getBooks = async () => {
    fetch("http://localhost:8088/api/v1/books").then(async (result) => {
      const data = await result.json();
      setBookCount(data.length || 0);
    });
  };

  const getUsers = async () => {
    fetch("http://localhost:8088/api/v1/clients").then(async (result) => {
      const data = await result.json();
      setUserCount(data.length || 0);
    });
  };

  const getRents = async () => {
    fetch("http://localhost:8088/api/v1/borrowings").then(async (result) => {
      const data = await result.json();
      setRentCount(data.length || 0);
    });
  };

  return (
    <Stack direction="column">
      <Typography variant="h4">Basic stats</Typography>
      <List>
        <ListItem>
          <ListItemIcon>
            <LibraryBooksIcon />
          </ListItemIcon>
          <ListItemText>Total Books {bookCount}</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText>Total Users {userCount}</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <BookIcon />
          </ListItemIcon>
          <ListItemText>Total rents {rentCount}</ListItemText>
        </ListItem>
      </List>
    </Stack>
  );
};

export default IndexPage;

export const Head: HeadFC = () => <title>Overview</title>;
