import * as React from "react";
import { Stack, Typography, Button } from "@mui/material";
import { HeadFC } from "gatsby";
import { FormContainer, TextFieldElement, useForm } from "react-hook-form-mui";
import { Store } from "react-notifications-component";
import {
  successNotification,
  errorNotification,
} from "../components/Notifications";

type Inputs = {
  forename: string;
  surname: string;
};

const RegisterUser = () => {
  const formContext = useForm<{ forename: string; surname: string }>({
    defaultValues: {
      forename: "",
      surname: "",
    },
  });

  const handleSubmit = (data: Inputs) => {
    fetch("http://localhost:8088/api/v1/clients", {
      method: "POST",
      body: JSON.stringify({ ...data }),
      headers: {
        "Content-Type": "application/json",
        Accepts: "application/json",
      },
    })
      .then((result) => {
        if (result.ok) {
          Store.addNotification({
            ...successNotification,
            message: "User added",
          });
          resetForm();
        } else {
          Store.addNotification({
            ...errorNotification,
            message: "Server error",
          });
        }
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "Error connecting to server",
        });
      });
  };

  const resetForm = () => {
    formContext.reset();
  };

  return (
    <>
      <Stack direction="column" justifyContent="center">
        <Typography variant="h4" sx={{ mb: 2 }}>
          Register new User
        </Typography>
        <FormContainer formContext={formContext} onSuccess={handleSubmit}>
          <Stack spacing={2}>
            <TextFieldElement
              required
              label="First name"
              name="forename"
              placeholder="John"
              validation={{
                required: true,
                minLength: 2,
              }}
            />
            <TextFieldElement
              required
              label="Second name"
              name="surname"
              placeholder="Lemon"
              validation={{
                required: true,
                minLength: 2,
              }}
            />
            <Button type="submit">Register</Button>
          </Stack>
        </FormContainer>
      </Stack>
    </>
  );
};

export default RegisterUser;

export const Head: HeadFC = () => <title>User Registration</title>;
