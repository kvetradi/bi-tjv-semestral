import * as React from "react";
import {
  Card,
  CardContent,
  CardHeader,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import BorrowingDetail from "../../components/BorrowingDetail";
import { IApiBorrowing } from "../../components/apiInterfaces";

const New = () => {
  const [borrowings, setBorrowings] = useState<Array<IApiBorrowing>>([]);
  const [openDetail, setOpenDetail] = useState<number | null>(null);
  const [isOnlyUnreturned, setIsOnlyUnreturned] = useState(false);

  useEffect(() => {
    getBorrowings();
  }, []);

  const getBorrowings = () => {
    fetch("http://localhost:8088/api/v1/borrowings", { method: "GET" })
      .then(async (result) => {
        const data: Array<IApiBorrowing> = await result.json();
        data.sort((a, b) => {
          return a.id - b.id;
        });
        setBorrowings(data);
      })
      .catch(() => {
        console.error("Error fetching resource");
      });
  };

  const openBorrowing = (index: number) => {
    setOpenDetail(index);
  };

  const updatedBorrowing = (newValue: IApiBorrowing) => {
    setBorrowings((prevState) =>
      prevState.map((borrowing) => {
        if (borrowing.id === newValue.id) {
          return newValue;
        } else return borrowing;
      })
    );
  };

  const deleteBorrowing = (toDelete: IApiBorrowing) => {
    setBorrowings((prevState) =>
      prevState.filter((borrowing) => borrowing.id !== toDelete.id)
    );
  }

  return (
    <>
      {openDetail && (
        <BorrowingDetail
          index={openDetail}
          onClose={() => setOpenDetail(null)}
          onUpdate={updatedBorrowing}
          onDelete={(toDelete) => {
            setOpenDetail(null);
            deleteBorrowing(toDelete);
          }}
        />
      )}
      <Stack direction="row" justifyContent="space-between">
        <Typography
          variant="h4"
          sx={{
            mb: 1.5,
          }}
        >
          All Borrowings
        </Typography>
        <Stack direction="row" alignItems="center">
          <Typography variant="subtitle2">Only Not returned</Typography>
          <Switch onClick={() => setIsOnlyUnreturned((prev) => !prev)} />
        </Stack>
      </Stack>
      <Stack
        justifyContent="center"
        direction="row"
        alignContent="center"
        gap={1.5}
        sx={{
          flexWrap: "wrap",
        }}
      >
        {borrowings
          .filter((b) => {
            return isOnlyUnreturned ? b.returnDate === null : true;
          })
          .map((borrowing) => (
            <Card
              sx={{
                width: "358px",
              }}
              key={borrowing.id}
            >
              <CardHeader
                title={`Borrowing #${borrowing.id}`}
                subheader={`Borrowed by ${borrowing.client.forename} ${borrowing.client.surname}`}
                onClick={() => openBorrowing(borrowing.id)}
                sx={{
                  cursor: "pointer",
                }}
              />
              <CardContent>
                <Stack direction="column">
                  <Stack direction="row" justifyContent="space-between">
                    <Typography variant="subtitle2">
                      From: {new Date(borrowing.borrowDate).toDateString()}
                    </Typography>
                    <Typography variant="subtitle2">
                      To:{" "}
                      {borrowing.returnDate === null
                        ? "-"
                        : new Date(borrowing.returnDate).toDateString()}
                    </Typography>
                  </Stack>
                  <Typography variant="body2">
                    Fee: {borrowing.fee} EUR
                  </Typography>
                  <Typography variant="body2">
                    Note: {borrowing.note}
                  </Typography>
                  <Typography variant="subtitle2">
                    Borrowed books: {borrowing.borrowed.length}
                  </Typography>
                </Stack>
              </CardContent>
            </Card>
          ))}
      </Stack>
    </>
  );
};

export default New;
