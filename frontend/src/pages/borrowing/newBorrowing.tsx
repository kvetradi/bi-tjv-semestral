import * as React from "react";
import {Button, Stack, Typography} from "@mui/material";
import {HeadFC} from "gatsby";
import {
  AutocompleteElement,
  DatePickerElement,
  FormContainer,
  TextFieldElement,
  useForm,
} from "react-hook-form-mui";
import {useEffect, useState} from "react";
import {Store} from "react-notifications-component";
import DateFnsProvider from "../../components/DateFnsProvider";
import {
  errorNotification,
  successNotification,
} from "../../components/Notifications";
import {IApiUser, IApiBook} from "../../components/apiInterfaces";

interface IOptionBook {
  id: number;
  value: IApiBook;
  label: string;
}

interface IFormInterface {
  client: IApiUser;
  books: Array<IOptionBook>;
  borrowDate: dateFns;
  returnDate: dateFns;
  note: string;
  fee: number;
}

const NewBorrowing = () => {
  const [userList, setUserList] = useState<Array<IApiUser>>([]);
  const [bookList, setBookList] = useState<Array<IApiBook>>([]);

  const formContext = useForm<IFormInterface>({
    defaultValues: {
      client: undefined,
      books: [],
      borrowDate: undefined,
      returnDate: undefined,
      note: "",
      fee: 0,
    },
  });

  useEffect(() => {
    getUsers();
    getBooks();
  }, []);

  const getUsers = () => {
    fetch("http://localhost:8088/api/v1/clients", {
      method: "GET",
    }).then(async (result) => {
      const data = await result.json();
      setUserList(
        data.map(
          (client: { forename: string; surname: string; id: number }) => ({
            id: client.id,
            firstName: client.forename,
            secondName: client.surname,
          })
        )
      );
    });
  };

  const getBooks = () => {
    fetch("http://localhost:8088/api/v1/books/free", {method: "GET"}).then(
      async (result) => {
        const data = await result.json();
        setBookList(data);
      }
    );
  };

  const handleSubmit = (data: IFormInterface) => {
    const dataToSend = {
      borrowed: data.books.map((book: IOptionBook) => ({
        id: book.value.id,
        name: "",
        author: "",
      })),
      borrowDate: data.borrowDate,
      returnDate: data.returnDate,
      note: data.note,
      fee: data.fee,
      client: data.client
    };
    fetch("http://localhost:8088/api/v1/borrowings", {
      method: "POST",
      body: JSON.stringify(dataToSend),
      headers: {
        "Content-Type": "application/json",
        Accepts: "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          Store.addNotification({
            ...successNotification,
            message: "Borrowing created",
          });
          formContext.reset();
          getBooks();
        } else {
          Store.addNotification({
            ...errorNotification,
            message: "Error connecting to server",
          });
        }
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "Server returned error, contact our support",
        });
      });
  };

  return (
    <>
      <Stack direction="column">
        <Typography variant="h4" sx={{mb: 2}}>
          New renting
        </Typography>
        <DateFnsProvider>
          <FormContainer
            onSuccess={handleSubmit}
            FormProps={{
              "aria-autocomplete": "none",
            }}
            formContext={formContext}
          >
            <Stack spacing={2}>
              <AutocompleteElement
                name="client"
                label="Client who is borrowing"
                options={userList.map((user) => ({
                  id: user.id,
                  label: `${user.id} - ${user.firstName} ${user.secondName}`,
                  value: user,
                }))}
                rules={{
                  required: true,
                }}
              />
              <AutocompleteElement
                multiple
                name="books"
                label="books which is he borrowing (multiselect)"
                options={bookList.map((book) => ({
                  id: book.id,
                  label: `${book.name} | by ${book.authorName}`,
                  value: book,
                }))}
                rules={{
                  required: true,
                }}
              />
              <Stack direction="row" justifyContent="space-between">
                <DatePickerElement
                  name="borrowDate"
                  label="from"
                  validation={{
                    required: true,
                  }}
                />
                <DatePickerElement name="returnDate" label="to"/>
              </Stack>
              <TextFieldElement name="note" label="Note"/>
              <TextFieldElement
                required
                label="fee"
                name="fee"
                type="number"
                placeholder="0"
                validation={{
                  required: true,
                  min: 0,
                }}
              />
              <Button type="submit">Submit</Button>
            </Stack>
          </FormContainer>
        </DateFnsProvider>
      </Stack>
    </>
  );
};

export default NewBorrowing;

export const Head: HeadFC = () => <title>Rent Management</title>;
