import * as React from "react";
import {
  CircularProgress,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Store } from "react-notifications-component";
import { IApiBorrowing } from "../../components/apiInterfaces";
import { errorNotification } from "../../components/Notifications";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import { HeadFC } from "gatsby";

const sortOptions = [
  "borrowingId",
  "bookName",
  "bookAuthor",
  "borrowDate",
  "returnDate",
  "clientFullName",
];

interface IBookDetail {
  [key: string]: any;
  borrowingId: number;
  borrowDate: string | number;
  returnDate: string | number;
  note: string;
  fee: number;
  clientId: number;
  clientFullName: string;
  bookId: number;
  bookName: string;
  bookAuthor: string;
}

interface IHeaderCell {
  name: string;
  onClick: (newState: string) => void;
  sorter: string;
  order: boolean;
}

const CustomTableHeaderCell = ({
  name,
  onClick,
  sorter,
  order,
}: IHeaderCell) => {
  return (
    <TableCell
      onClick={() => {
        onClick(name);
      }}
      sx={{
        cursor: "pointer",
      }}
    >
      <Stack direction="row">
        {name}
        {sorter === name &&
          (order ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />)}
      </Stack>
    </TableCell>
  );
};

const BookNotes = () => {
  const [information, setInformation] = useState<Array<IBookDetail>>([]);
  const [sorter, setSorter] = useState("borrowingId");
  const [order, setOrder] = useState(true);

  const getInformation = async () => {
    fetch("http://localhost:8088/api/v1/borrowings/withNotes", {
      method: "GET",
    })
      .then(async (result) => {
        const data: Array<IApiBorrowing> = await result.json();
        data.sort((a, b) => {
          return a.id - b.id;
        });
        let newData: Array<IBookDetail> = [];
        data.forEach((borrowing) => {
          borrowing.borrowed.forEach((book) => {
            newData.push({
              borrowingId: borrowing.id,
              borrowDate: borrowing.borrowDate,
              returnDate: borrowing.returnDate,
              note: borrowing.note,
              fee: borrowing.fee,
              clientId: borrowing.client.id,
              clientFullName: `${borrowing.client.forename} ${borrowing.client.surname}`,
              bookId: book.id,
              bookName: book.name,
              bookAuthor: book.authorName,
            });
          });
        });
        setInformation(newData);
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "error occurred while getting data from server",
        });
      });
  };

  const changeSorter = (newSorter: string) => {
    if (!sortOptions.includes(newSorter)) return;
    if (sorter === newSorter) {
      setOrder((prev) => !prev);
      return;
    }
    setSorter(newSorter);
    setOrder(true);
  };

  useEffect(() => {
    getInformation();
  }, []);

  return (
    <Stack>
      <Typography variant="h5" sx={{ pb: 2 }}>
        Books with Notes
      </Typography>
      {information.length == 0 && <CircularProgress />}
      {information.length > 0 && (
        <Table>
          <TableHead>
            <TableRow>
              <CustomTableHeaderCell
                name={"borrowingId"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <CustomTableHeaderCell
                name={"bookName"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <CustomTableHeaderCell
                name={"bookAuthor"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <CustomTableHeaderCell
                name={"borrowDate"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <CustomTableHeaderCell
                name={"returnDate"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <CustomTableHeaderCell
                name={"clientFullName"}
                onClick={changeSorter}
                sorter={sorter}
                order={order}
              />
              <TableCell sx={{maxWidth: "100px"}}>Note</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {information
              .sort((a, b) => {
                if (a[sorter] < b[sorter]) return order ? -1 : 1;
                if (b[sorter] < a[sorter]) return order ? 1 : -1;
                return 0;
              })
              .map((bookDetail) => (
                <TableRow
                  key={`${bookDetail.borrowingId}-${bookDetail.bookId}-${bookDetail.clientId}`}
                >
                  <TableCell>{bookDetail.borrowingId}</TableCell>
                  <TableCell>{bookDetail.bookName}</TableCell>
                  <TableCell>{bookDetail.bookAuthor}</TableCell>
                  <TableCell>
                    {new Date(bookDetail.borrowDate).toDateString()}
                  </TableCell>
                  <TableCell>
                    {new Date(bookDetail.returnDate).toDateString()}
                  </TableCell>
                  <TableCell>{bookDetail.clientFullName}</TableCell>
                  <TableCell sx={{maxWidth: "100px", overflow: "auto"}}>{bookDetail.note}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      )}
    </Stack>
  );
};

export default BookNotes;

export const Head: HeadFC = () => <title>Notes</title>;
