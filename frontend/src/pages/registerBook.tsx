import * as React from "react";
import { Button, Stack, Typography } from "@mui/material";
import { HeadFC } from "gatsby";
import {
  DatePickerElement,
  FormContainer,
  TextFieldElement,
  useForm,
} from "react-hook-form-mui";
import DateFnsProvider from "../components/DateFnsProvider";
import { Store } from "react-notifications-component";
import {
  successNotification,
  errorNotification,
} from "../components/Notifications";

type Inputs = {
  name: string;
  numberOfPages: number;
  releaseDate: string;
  authorName: string;
};

const RegisterBook = () => {
  const formContext = useForm<{
    name: string;
    numberOfPages: number;
    releaseDate: string;
    authorName: string;
  }>({
    defaultValues: {
      name: "",
      numberOfPages: undefined,
      releaseDate: "",
      authorName: "",
    },
  });

  const handleSubmit = (data: Inputs) => {
    fetch("http://localhost:8088/api/v1/books", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Accepts: "application/json",
      },
    })
      .then((result) => {
        if (result.ok) {
          Store.addNotification({
            ...successNotification,
            message: "Book added",
          });
          formContext.reset();
        } else {
          Store.addNotification({
            ...errorNotification,
            message: "Server error",
          });
        }
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "Error connecting to server",
        });
      });
  };

  return (
    <>
      <Stack direction="column" justifyContent="center">
        <Typography variant="h4" sx={{ mb: 2 }}>
          Register new Book
        </Typography>
        <DateFnsProvider>
          <FormContainer
            formContext={formContext}
            onSuccess={handleSubmit}
            FormProps={{
              "aria-autocomplete": "none",
            }}
          >
            <Stack spacing={2}>
              <TextFieldElement
                required
                label="Book name"
                name="name"
                placeholder="The best book"
                validation={{
                  required: true,
                  minLength: 2,
                }}
              />
              <TextFieldElement
                required
                label="number of pages"
                name="numberOfPages"
                type="number"
                placeholder="5"
                validation={{
                  required: true,
                  min: 1,
                }}
              />
              <TextFieldElement
                label="Author name"
                name="authorName"
                placeholder="John Lemon"
              />
              <DatePickerElement
                isDate
                label="Release date"
                name="releaseDate"
                inputFormat="dd.MM.yyyy"
              />
              <Button type="submit">Register</Button>
            </Stack>
          </FormContainer>
        </DateFnsProvider>
      </Stack>
    </>
  );
};

export default RegisterBook;

export const Head: HeadFC = () => <title>Book Registration</title>;
