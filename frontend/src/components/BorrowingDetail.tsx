import * as React from "react";
import {useEffect, useState} from "react";
import {
  Button,
  List,
  ListItem,
  Modal,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import {Store} from "react-notifications-component";
import {errorNotification, successNotification} from "./Notifications";
import {IApiBorrowing} from "./apiInterfaces";
import {FormContainer, TextFieldElement} from "react-hook-form-mui";

interface IBorrowingDetail {
  index: number;
  onClose: () => void | undefined;
  onUpdate: (value: IApiBorrowing) => void | undefined;
  onDelete: (value: IApiBorrowing) => void | undefined;
}

interface IFormInterface {
  fee: number;
  note: string;
}

const BorrowingDetail = ({index, onClose, onUpdate, onDelete}: IBorrowingDetail) => {
  const [detail, setDetail] = useState<IApiBorrowing | undefined>(undefined);
  const [isForm, setIsForm] = useState(false);
  const [isChanged, setIsChanged] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  useEffect(() => {
    getDetail();
  }, []);

  const getDetail = async () => {
    fetch(`http://localhost:8088/api/v1/borrowings/${index}`, {
      method: "GET",
    }).then(async (result) => {
      const data: IApiBorrowing = await result.json();
      setDetail(data);
    });
  };

  const putUpdate = async () => {
    if (detail === undefined) return;
    fetch(`http://localhost:8088/api/v1/borrowings/${index}`, {
      method: "PUT",
      body: JSON.stringify(detail),
      headers: {
        "Content-Type": "application/json",
        Accepts: "application/json",
      },
    })
      .then((result) => {
        if (result.ok) {
          Store.addNotification({
            ...successNotification,
            message: "Update was successful",
          });
        } else {
          Store.addNotification({
            ...errorNotification,
            message: "Something went wrong during update",
          });
        }
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "Something ent wrong on server",
        });
      });
  };

  const deleteBorrowing = async () => {
    if (detail === undefined) return;
    fetch(`http://localhost:8088/api/v1/borrowings/${index}`, {
      method: "DELETE",
    })
      .then((result) => {
        if (result.ok) {
          Store.addNotification({
            ...successNotification,
            message: "Delete was successful",
          });
        } else {
          Store.addNotification({
            ...errorNotification,
            message: "Something went wrong during update",
          });
        }
      })
      .catch(() => {
        Store.addNotification({
          ...errorNotification,
          message: "Something ent wrong on server",
        });
      });
  }

  const handleReturnNow = async () => {
    setDetail((prevState) => {
      if (prevState === undefined) return undefined;
      return {
        ...prevState,
        returnDate: Date.now(),
      };
    });
    setIsChanged(true);
  };

  const handleChange = (data: IFormInterface) => {
    setIsForm(false);
    setDetail((prevState) => {
      return prevState === undefined
        ? undefined
        : {...prevState, note: data.note, fee: data.fee};
    });
    setIsChanged(true);
  };

  const beforeClose = async () => {
    if (isChanged && detail !== undefined) {
      await putUpdate();
      onUpdate(detail);
    }
  };

  return (
    <Modal open={true} onClose={onClose}>
      <Paper
        sx={{
          position: "absolute" as "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: `${isMobile ? "90vw" : "700px"}`,
          height: "500px",
          p: 2,
        }}
      >
        {detail && (
          <Stack
            direction="column"
            alignItems="center"
            justifyContent="space-between"
            sx={{
              height: "100%",
            }}
            spacing={2}
          >
            <Stack direction="column" alignItems="center">
              <Typography variant="h6">Borrowing #{detail.id}</Typography>
              <Typography variant="subtitle1">
                Borrowed by {`${detail.client.forename} ${detail.client.surname}`}
              </Typography>
              <Typography variant="subtitle2">
                Borrowed on {new Date(detail.borrowDate).toDateString()}
              </Typography>
              <Typography variant="subtitle2">
                {detail.returnDate === null
                  ? "Not returned"
                  : `Returned on ${new Date(detail.returnDate).toDateString()}`}
              </Typography>
              {!isForm && (
                <>
                  <Typography
                    variant="subtitle1"
                    onClick={() => {
                      setIsForm(true);
                    }}
                  >
                    Fee: {detail.fee}
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    sx={{overflow: "auto"}}
                    onClick={() => {
                      setIsForm(true);
                    }}
                  >
                    Note: {detail.note.length === 0 ? "-" : detail.note}
                  </Typography>
                </>
              )}
              {isForm && (
                <>
                  <FormContainer
                    defaultValues={{
                      note: detail.note,
                      fee: detail.fee,
                    }}
                    onSuccess={handleChange}
                  >
                    <Stack direction="column" spacing={1}>
                      <TextFieldElement
                        required
                        label="fee"
                        name="fee"
                        type="number"
                        placeholder="0"
                        validation={{
                          required: true,
                          min: 0,
                        }}
                      />
                      <TextFieldElement name="note" label="Note"/>
                      <>
                        <Button
                          type="submit"
                          onClick={() => {
                            setIsForm(false);
                          }}
                        >
                          Cancel
                        </Button>
                        <Button type="submit">Change</Button>
                      </>
                    </Stack>
                  </FormContainer>
                </>
              )}
              {!detail.returnDate && (
                <Button
                  sx={{
                    mt: 2,
                  }}
                  onClick={handleReturnNow}
                >
                  Return now
                </Button>
              )}
            </Stack>

            <Stack
              sx={{
                height: "300px",
                width: "100%",
                overflow: "auto",
              }}
            >
              <List>
                {detail.borrowed.map((book) => (
                  <ListItem
                    sx={{
                      maxWidth: "100%",
                    }}
                    key={book.id}
                  >
                    {book.name} | by {book.authorName}
                  </ListItem>
                ))}
              </List>
            </Stack>

            <Stack direction="row" spacing={2}>
              <Button
                onClick={async () => {
                  await deleteBorrowing();
                  onDelete(detail);
                }}
                color="secondary"
              >
                Delete
              </Button>
              <Button
                onClick={async () => {
                  await beforeClose();
                  onClose();
                }}
              >
                Confirm
              </Button>
            </Stack>
          </Stack>
        )}
      </Paper>
    </Modal>
  );
};

export default BorrowingDetail;
