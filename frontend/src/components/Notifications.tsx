import * as React from "react";
import { iNotification } from "react-notifications-component";

export const successNotification: iNotification = {
  title: "Success",
  type: "success",
  message: "success",
  insert: "top",
  container: "top-right",
  animationIn: ["animate__animated", "animate__fadeIn"],
  animationOut: ["animate__animated", "animate__fadeOut"],
  dismiss: {
    duration: 5000,
    onScreen: true,
  },
};

export const errorNotification: iNotification = {
  title: "Error",
  type: "danger",
  message: "Error",
  insert: "top",
  container: "top-right",
  animationIn: ["animate__animated", "animate__fadeIn"],
  animationOut: ["animate__animated", "animate__fadeOut"],
  dismiss: {
    duration: 5000,
    onScreen: true,
  },
}
