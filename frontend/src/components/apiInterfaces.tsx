export interface IApiUser {
  id: number;
  firstName: string;
  secondName: string;
}

export interface IApiBook {
  id: number;
  name: string;
  numberOfPages: number;
  releaseDate: string;
  authorName: string;
}

export interface IApiBorrowing {
  id: number;
  borrowDate: string | number;
  returnDate: string | number;
  note: string;
  fee: number;
  borrowed: Array<IApiBook>;
  client: IApiClient
}
