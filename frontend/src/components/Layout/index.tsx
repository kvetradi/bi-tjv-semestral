import * as React from "react";
import { ReactNode, useState } from "react";
import {
  AppBar,
  Toolbar,
  Container,
  Stack,
  Menu,
  Box,
  MenuItem,
  Typography,
} from "@mui/material";
import { Button as LinkButton, IconButton } from "gatsby-theme-material-ui";
import LocalLibraryIcon from "@mui/icons-material/LocalLibrary";
import { ReactNotifications } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import "animate.css/animate.min.css";
import { Link } from "gatsby-link";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { ArrowDropUp } from "@mui/icons-material";
import { useTheme } from "@mui/material";

interface Props {
  children: ReactNode;
}

const links = [
  {
    link: "/",
    text: "Index",
  },
  {
    link: "/registerUser/",
    text: "Register User",
  },
  {
    link: "/registerBook/",
    text: "Register Book",
  },
];

export default function Layout({ children }: Props) {
  const [menuAnchor, setMenuAnchor] = useState<null | HTMLElement>(null);
  const theme = useTheme();

  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchor(event.currentTarget);
  };

  const closeMenu = () => {
    setMenuAnchor(null);
  };

  return (
    <>
      <ReactNotifications />
      <AppBar>
        <Container>
          <Toolbar disableGutters>
            <Stack direction="row" spacing={1}>
              <IconButton to="/">
                <LocalLibraryIcon color="secondary" />
              </IconButton>
              {links.map((item) => (
                <LinkButton to={item.link} variant="text" key={item.text}>
                  {item.text}
                </LinkButton>
              ))}
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                }}
              >
                <Typography onClick={openMenu} variant="subtitle2">
                  Borrowings
                </Typography>
                {menuAnchor === null ? <ArrowDropDownIcon /> : <ArrowDropUp />}
                <Menu
                  open={menuAnchor != null}
                  keepMounted
                  anchorEl={menuAnchor}
                  id="manage-options"
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  onClose={closeMenu}
                >
                  <MenuItem>
                    <Link
                      onClick={closeMenu}
                      to="/borrowing/newBorrowing"
                      activeStyle={{
                        color: "#D1AE04",
                      }}
                      style={{
                        color: theme.palette.grey["400"],
                      }}
                    >
                      New renting
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      onClick={closeMenu}
                      to="/borrowing/oldBorrowings"
                      activeStyle={{
                        color: "#D1AE04",
                      }}
                      style={{
                        color: theme.palette.grey["400"],
                      }}
                    >
                      View old
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      onClick={closeMenu}
                      to="/borrowing/bookNotes"
                      activeStyle={{
                        color: "#D1AE04",
                      }}
                      style={{
                        color: theme.palette.grey["400"],
                      }}
                    >
                      Notes
                    </Link>
                  </MenuItem>
                </Menu>
              </Box>
            </Stack>
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />
      <Container sx={{ pt: 2 }}>{children}</Container>
    </>
  );
}
