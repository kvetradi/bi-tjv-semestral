import { createTheme } from "@mui/material";

const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        "a:not([style]):not([class])": {
          color: "inherit",
        },
        a: {
          textDecoration: "none",
        },
      }
    },
    MuiIconButton: {
      styleOverrides: {
        root({ theme }) {
          return {
            borderRadius: theme.spacing(0.5),
          };
        },
      },
    },
    MuiButton: {
      defaultProps: {
        disableRipple: true,
        variant: "contained",
      },
      styleOverrides: {
        root({ theme }) {
          return {
            boxShadow: "none",
            borderRadius: theme.spacing(0.5),
            textTransform: "none",
            color: "white",
            background: theme.palette.primary,
          };
        },
      },
    },
    MuiSvgIcon: {
      styleOverrides: {
        colorSecondary: {
          color: "#FFF",
        },
      },
    },
  },
  palette: {
    primary: {
      main: "#1976D2",
    },
    secondary: {
      main: "#D1AE04",
    },
    background: {
      light: "#FFFFFF",
      default: "#F0F0F0",
    },
    error: {
      main: "#F44336",
    },
    text: {
      light: "#262626",
      primary: "#1A1917",
    },
    grey: {
      50: "#F0F0F0",
      100: "#CCCCCC",
      200: "#808080",
      300: "#4D4D4D",
      400: "#262626",
      500: "#1A1917",
    },
  },
});

export default theme;
