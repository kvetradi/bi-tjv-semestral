-- book insert

insert into book (id_book, name, num_pages, date_release, author) values (1, 'The Great Gatsby', 300, '1925-04-10', 'F. Scott Fitzgerald'),
  (2, 'To Kill a Mockingbird', 281, '1960-07-11', 'Harper Lee'),
  (3, '1984', 328, '1949-06-08', 'George Orwell'),
  (4, 'The Catcher in the Rye', 224, '1951-07-16', 'J.D. Salinger'),
  (5, 'Pride and Prejudice', 432, '1813-01-28', 'Jane Austen'),
  (6, 'The Hobbit', 310, '1937-09-21', 'J.R.R. Tolkien'),
  (7, 'Harry Potter and the Sorcerer''s Stone', 309, '1997-06-26', 'J.K. Rowling'),
  (8, 'The Da Vinci Code', 454, '2003-03-18', 'Dan Brown'),
  (9, 'The Lord of the Rings', 1178, '1954-07-29', 'J.R.R. Tolkien'),
  (10, 'The Hunger Games', 374, '2008-09-14', 'Suzanne Collins'),
  (11, 'The Shining', 447, '1977-01-28', 'Stephen King'),
  (12, 'Dune', 412, '1965-08-01', 'Frank Herbert'),
  (13, 'The Alchemist', 197, '1988-09-01', 'Paulo Coelho'),
  (14, 'The Girl with the Dragon Tattoo', 465, '2005-08-01', 'Stieg Larsson'),
  (15, 'Moby-Dick', 635, '1851-10-18', 'Herman Melville'),
  (16, 'The Odyssey', 454, NULL, 'Homer'),
  (17, 'The Road', 287, '2006-09-26', 'Cormac McCarthy'),
  (18, 'Brave New World', 311, '1932-05-14', 'Aldous Huxley'),
  (19, 'The Picture of Dorian Gray', 254, '1890-06-20', 'Oscar Wilde'),
  (20, 'The Grapes of Wrath', 464, '1939-04-14', 'John Steinbeck'),
  (21, 'The Outsiders', 192, '1967-04-24', 'S.E. Hinton'),
  (22, 'The Hitchhiker''s Guide to the Galaxy', 193, '1979-10-12', 'Douglas Adams'),
  (23, 'The Road Less Traveled', 316, '1978-03-23', 'M. Scott Peck'),
  (24, 'The Great Expectations', 505, '1861-08-30', 'Charles Dickens'),
  (25, 'The Sun Also Rises', 251, '1926-10-22', 'Ernest Hemingway');

SELECT setval('book_id_book_seq', 25);

-- client insert

insert into client (id_client, forename, date_registered, surname) values (1, 'Gordie', '2021-10-12', 'Rydeard');
insert into client (id_client, forename, date_registered, surname) values (2, 'Mae', '2021-04-23', 'Adamovich');
insert into client (id_client, forename, date_registered, surname) values (3, 'Francisca', '2022-07-10', 'Olivia');
insert into client (id_client, forename, date_registered, surname) values (4, 'Chip', '2021-09-18', 'Fathers');
insert into client (id_client, forename, date_registered, surname) values (5, 'Jameson', '2021-03-30', 'Anders');
insert into client (id_client, forename, date_registered, surname) values (6, 'Sloan', '2021-01-04', 'Langham');
insert into client (id_client, forename, date_registered, surname) values (7, 'Krystyna', '2021-12-22', 'Garnul');
insert into client (id_client, forename, date_registered, surname) values (8, 'Monte', '2021-09-15', 'Sobieski');
insert into client (id_client, forename, date_registered, surname) values (9, 'Mayer', '2022-04-24', 'Deschlein');
insert into client (id_client, forename, date_registered, surname) values (10, 'Jeno', '2022-11-03', 'Tellwright');

SELECT setval('client_id_client_seq', 10);

-- borrowing insert

insert into borrowing (id_borrowing, date_borrow, date_return, note, fee, id_client) values (1, '2022-09-04', null, 'Not returned yet', 72, 1);
insert into borrowing (id_borrowing, date_borrow, date_return, note, fee, id_client) values (2, '2021-01-12', '2022-04-02', 'Returned in amazing condition', 8, 10);
insert into borrowing (id_borrowing, date_borrow, date_return, note, fee, id_client) values (3, '2021-03-30', null, '', 58, 1);

SELECT setval('borrowing_id_borrowing_seq', 3);

-- book_borrowing insert

insert into book_borrowing (id_book, id_borrowing) values (1, 1);
insert into book_borrowing (id_book, id_borrowing) values (2, 1);
insert into book_borrowing (id_book, id_borrowing) values (1, 2);
insert into book_borrowing (id_book, id_borrowing) values (4, 2);
insert into book_borrowing (id_book, id_borrowing) values (2, 3);
insert into book_borrowing (id_book, id_borrowing) values (3, 3);
insert into book_borrowing (id_book, id_borrowing) values (1, 3);
