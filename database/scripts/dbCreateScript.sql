-- odeberu pokud existuje funkce na oodebrání tabulek a sekvencí
DROP FUNCTION IF EXISTS remove_all();

-- vytvořím funkci která odebere tabulky a sekvence
-- chcete také umět psát PLSQL? Zapište si předmět BI-SQL ;-)
CREATE or replace FUNCTION remove_all() RETURNS void AS $$
DECLARE
    rec RECORD;
    cmd text;
BEGIN
    cmd := '';

    FOR rec IN SELECT
            'DROP SEQUENCE ' || quote_ident(n.nspname) || '.'
                || quote_ident(c.relname) || ' CASCADE;' AS name
        FROM
            pg_catalog.pg_class AS c
        LEFT JOIN
            pg_catalog.pg_namespace AS n
        ON
            n.oid = c.relnamespace
        WHERE
            relkind = 'S' AND
            n.nspname NOT IN ('pg_catalog', 'pg_toast') AND
            pg_catalog.pg_table_is_visible(c.oid)
    LOOP
        cmd := cmd || rec.name;
    END LOOP;

    FOR rec IN SELECT
            'DROP TABLE ' || quote_ident(n.nspname) || '.'
                || quote_ident(c.relname) || ' CASCADE;' AS name
        FROM
            pg_catalog.pg_class AS c
        LEFT JOIN
            pg_catalog.pg_namespace AS n
        ON
            n.oid = c.relnamespace WHERE relkind = 'r' AND
            n.nspname NOT IN ('pg_catalog', 'pg_toast') AND
            pg_catalog.pg_table_is_visible(c.oid)
    LOOP
        cmd := cmd || rec.name;
    END LOOP;

    EXECUTE cmd;
    RETURN;
END;
$$ LANGUAGE plpgsql;
-- zavolám funkci co odebere tabulky a sekvence - Mohl bych dropnout celé schéma a znovu jej vytvořit, použíjeme však PLSQL
select remove_all();

-- Remove conflicting tables
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS borrowing CASCADE;
DROP TABLE IF EXISTS client CASCADE;
DROP TABLE IF EXISTS book_borrowing CASCADE;
-- End of removing

CREATE TABLE book (
    id_book SERIAL NOT NULL,
    name VARCHAR(1000) NOT NULL,
    num_pages INTEGER NOT NULL,
    date_release DATE,
    author VARCHAR(256)
);
ALTER TABLE book ADD CONSTRAINT pk_book PRIMARY KEY (id_book);

CREATE TABLE borrowing (
    id_borrowing SERIAL NOT NULL,
    id_client INTEGER NOT NULL,
    date_borrow DATE NOT NULL,
    date_return DATE,
    note VARCHAR(512) NOT NULL,
    fee INTEGER NOT NULL
);
ALTER TABLE borrowing ADD CONSTRAINT pk_borrowing PRIMARY KEY (id_borrowing);

CREATE TABLE client (
    id_client SERIAL NOT NULL,
    forename VARCHAR(100) NOT NULL,
    surname VARCHAR(100) NOT NULL,
    date_registered TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
ALTER TABLE client ADD CONSTRAINT pk_client PRIMARY KEY (id_client);

CREATE TABLE book_borrowing (
    id_book INTEGER NOT NULL,
    id_borrowing INTEGER NOT NULL
);
ALTER TABLE book_borrowing ADD CONSTRAINT pk_book_borrowing PRIMARY KEY (id_book, id_borrowing);

ALTER TABLE borrowing ADD CONSTRAINT fk_borrowing_client FOREIGN KEY (id_client) REFERENCES client (id_client) ON DELETE CASCADE;

ALTER TABLE book_borrowing ADD CONSTRAINT fk_book_borrowing_book FOREIGN KEY (id_book) REFERENCES book (id_book) ON DELETE CASCADE;
ALTER TABLE book_borrowing ADD CONSTRAINT fk_book_borrowing_borrowing FOREIGN KEY (id_borrowing) REFERENCES borrowing (id_borrowing) ON DELETE CASCADE;
