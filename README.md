# Library

## Usage

To run the semestral you do not need any other repository.

You can run 2 parts in docker, that is the database, which will be initialized with 10 books, 10 clients and 3 borrowings.
The server and API are needed to be run using IDE or JAVA executable.

Database and client:
- Using `docker-compose.yml` you can run services for database and client

API:
- Build it with gradle and run it with Java

## Description

Simple library with clients, books, employees and renting.
In this library people are allowed to rent a book, and later return it.
If there is something bad with the book on return the staff who checks it can make a note about it.

## Business operations

- Creation of new borrowing, where you can specify which books the client has borrowed and if it was already returned or not.
  - books which are added can not be already borrowed by someone else

## More complex query

Select all books which have some notes in renting and display for which client the notes are
- this means that we will see all the books which have some notes from borrowing table, and with that we will see who had them borrowed in that borrowing

## database

![database](/database.png)

