package cz.cvut.fit.tjv.kvetradi.business;

import java.util.Optional;

public interface AbstractCrudService<E, K> {
    /**
     * Create new entity in the repository
     *
     * @param entity entity to create
     * @return created entity
     * @throws EntityStateException if entity is already present in the repository
     */
    E create(E entity);

    /**
     * Update given entity in repository.
     *
     * @param entity entity to update
     * @return new updated entity
     * @throws EntityStateException if entity does not exist in repository
     */
    E update(E entity);

    /**
     * Update given entity in repository.
     *
     * @param id entity id to update
     * @param e  entity to update
     * @return new updated entity
     * @throws EntityStateException if entity does not exist in repository
     */
    E update(K id, E e);

    /**
     * Get entity by with given id if it exists
     *
     * @param id ID of the entity we want
     * @return Optional of Entity with value of found entity, if not found null is present
     */
    Optional<E> readByID(K id);

    /**
     * Get all entities in the repository
     *
     * @return Iterable of all found entities in the repository
     */
    Iterable<E> readAll();

    /**
     * Delete entity with given id from repository
     *
     * @param key id of the entity we want to delete
     */
    void deleteById(K key);

    /**
     * Delete provided entity if it exists in repository
     *
     * @param entity entity to delete
     */
    void delete(E entity);
}
