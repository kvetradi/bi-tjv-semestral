package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.book.BookDto;
import cz.cvut.fit.tjv.kvetradi.domain.Book;

import java.util.Collection;

public interface BookController extends AbstractController<Book, BookDto, Long> {
    /**
     * Get all not borrowed books.
     * @return Collection of all the books which are not borrowed.
     */
    Collection<BookDto> readAllFree();
    /**
     * Get hte number of books in our system.
     * @return number of books our system has.
     */
    Long count();
}
