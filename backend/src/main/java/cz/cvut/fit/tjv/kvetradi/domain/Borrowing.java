package cz.cvut.fit.tjv.kvetradi.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "borrowing")
public class Borrowing implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_borrowing")
    private long id;

    @Column(nullable = false, name = "date_borrow")
    private Date borrowDate;
    @Column(name = "date_return")
    private Date returnDate;
    @Column(nullable = false)
    private String note;
    @Column(nullable = false)
    private int fee;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_borrowing",
            joinColumns = @JoinColumn(name = "id_borrowing", referencedColumnName = "id_borrowing"),
            inverseJoinColumns = @JoinColumn(name = "id_book", referencedColumnName = "id_book")
    )
    private Collection<Book> borrowed;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_client")
    private Client client;

    public Borrowing(long id, Date borrowDate, Date returnDate, String note, int fee, Client client, Collection<Book> borrowed) {
        this.id = id;
        this.borrowDate = borrowDate;
        this.returnDate = returnDate;
        this.note = note;
        this.fee = fee;
        this.borrowed = borrowed;
        this.client = client;
    }

    public Borrowing(Date borrowDate, Date returnDate, String note, int fee, Client client, Collection<Book> borrowed) {
        this.borrowDate = borrowDate;
        this.returnDate = returnDate;
        this.note = note;
        this.fee = fee;
        this.borrowed = borrowed;
        this.client = client;
    }

    /**
     * Create new borrowing with all attributes
     * @param borrowDate date when the borrow started; can not be NULL
     * @param returnDate date when the borrow ended; can be NULL
     * @param note note for the borrowing; can be NULL
     * @param fee fee of the borrowing
     * @param client client who this borrowing belongs to
     * @throws NullPointerException if borrowDate or client is null
     */
    public Borrowing(Date borrowDate, Date returnDate, String note, int fee, Client client) {
        this.borrowDate = Objects.requireNonNull(borrowDate);
        this.returnDate = returnDate;
        this.note = Objects.requireNonNull(note);
        this.fee = fee;
        this.borrowed = new ArrayList<>();
        this.client = Objects.requireNonNull(client);
    }

    /**
     * Create new borrowing without client
     * @param borrowDate when the borrowing started
     * @param returnDate when it was returned
     * @param note note for the borrowing
     * @param fee fee of the borrowing
     */
    public Borrowing(Date borrowDate, Date returnDate, String note, int fee) {
        this.borrowDate = Objects.requireNonNull(borrowDate);
        this.returnDate = returnDate;
        this.note = Objects.requireNonNull(note);
        this.fee = fee;
        this.borrowed = new ArrayList<>();
        this.client = null;
    }

    /**
     * Create new empty borrowing
     */
    public Borrowing() {
        this(new Date(), null, "", 0);
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public Collection<Book> getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(Collection<Book> borrowed) {
        this.borrowed = borrowed;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Borrowing borrowing = (Borrowing) o;
        return id == borrowing.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
