package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.BookJpaRepository;
import cz.cvut.fit.tjv.kvetradi.dao.jpa.BorrowingJpaRepository;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class BorrowingServiceImpl extends AbstractCrudServiceImpl<Borrowing, Long> implements BorrowingService {
    private final BorrowingJpaRepository borrowingJpaRepository;
    private final BookJpaRepository bookJpaRepository;

    public BorrowingServiceImpl(BorrowingJpaRepository repository,
                                BookJpaRepository bookJpaRepository) {
        this.borrowingJpaRepository = repository;
        this.bookJpaRepository = bookJpaRepository;
    }

    public Collection<Borrowing> findReturned() {
        return borrowingJpaRepository.findAllByReturnDateNotNull();
    }

    public Collection<Borrowing> findNotReturned() {
        return borrowingJpaRepository.findAllByReturnDateNull();
    }

    public Collection<Borrowing> findWithNotes() {
        return borrowingJpaRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty();
    }

    @Override
    protected JpaRepository<Borrowing, Long> getRepository() {
        return borrowingJpaRepository;
    }

    @Override
    public Long count() {
        return getRepository().count();
    }

    @Override
    public Collection<Borrowing> findBorrowingsForBook(Long id) {
        if (id == null || !bookJpaRepository.existsById(id)) {
            throw new EntityStateException();
        }
        return borrowingJpaRepository.findDistinctByBorrowedId(id);
    }
}
