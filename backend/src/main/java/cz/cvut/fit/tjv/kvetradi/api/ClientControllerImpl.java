package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.client.ClientDto;
import cz.cvut.fit.tjv.kvetradi.business.AbstractCrudService;
import cz.cvut.fit.tjv.kvetradi.business.ClientService;
import cz.cvut.fit.tjv.kvetradi.domain.Client;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.function.Function;

@RestController
@RequestMapping("/clients")
@Tag(name = "Clients", description = "Client REST API endpoint")
public class ClientControllerImpl extends AbstractControllerImpl<Client, ClientDto, Long> implements ClientController{
    private final ClientService clientService;

    public ClientControllerImpl(ClientService clientService, Function<Client, ClientDto> toDtoConverter, Function<ClientDto, Client> toEntityConverter) {
        super(toDtoConverter, toEntityConverter);
        this.clientService = clientService;
    }

    @Override
    protected AbstractCrudService<Client, Long> getService() {
        return clientService;
    }

    @Override
    @GetMapping("/count")
    public Long count() {
        return clientService.count();
    }
}
