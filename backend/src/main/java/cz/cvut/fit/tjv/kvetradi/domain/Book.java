package cz.cvut.fit.tjv.kvetradi.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "book")
public class Book implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private long id;

    @Column(nullable = false)
    private String name = "";
    @Column(nullable = false, name = "num_pages")
    private int numberOfPages = 0;
    @Column(name = "date_release")
    private Date releaseDate;
    @Column(name = "author")
    private String authorName;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_borrowing",
            joinColumns = @JoinColumn(name = "id_book", referencedColumnName = "id_book"),
            inverseJoinColumns = @JoinColumn(name = "id_borrowing", referencedColumnName = "id_borrowing")
    )
    private Collection<Borrowing> borrowing;

    /**
     * Create new Book instance with all parameters including ID
     * @param id id of the book
     * @param name          Name of the book; can not be NULL
     * @param numberOfPages Number of pages the book has
     * @param releaseDate   Release date of the book; can be NULL
     * @param authorName    Author name of this book; can be NULL
     * @throws NullPointerException If name is NULL
     */

    public Book(long id, String name, int numberOfPages, Date releaseDate, String authorName) {
        this.id = id;
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.releaseDate = releaseDate;
        this.authorName = authorName;
        this.borrowing = new ArrayList<>();
    }

    /**
     * Create new Book instance with all attributes
     *
     * @param name          Name of the book; can not be NULL
     * @param numberOfPages Number of pages the book has
     * @param releaseDate   Release date of the book; can be NULL
     * @param authorName    Author name of this book; can be NULL
     * @throws NullPointerException If name is NULL
     */
    public Book(String name, int numberOfPages, Date releaseDate, String authorName) {
        this.name = Objects.requireNonNull(name);
        this.numberOfPages = numberOfPages;
        this.releaseDate = releaseDate;
        this.authorName = authorName;
        this.borrowing = new ArrayList<>();
    }

    /**
     * Create new empty book
     */
    public Book() {
        this.id = 0L;
        this.name = "";
        this.numberOfPages = 0;
        this.releaseDate = new Date();
        this.authorName = "";
        this.borrowing = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Collection<Borrowing> getBorrowing() {
        return borrowing;
    }

    public void setBorrowing(Collection<Borrowing> borrowing) {
        this.borrowing = borrowing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
