package cz.cvut.fit.tjv.kvetradi.domain;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "client")
public class Client implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    private long id;

    @Column(nullable = false)
    private String forename;
    @Column(nullable = false)
    private String surname;
    /*
    Both @Generated(GenerationTime.ALWAYS) and @CreationTimestamp work here
     */
    @Column(name = "date_registered")
    @CreationTimestamp
    private Date registerDate;
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private Collection<Borrowing> myBorrowings;

    /**
     * Create new client with all parameters and empty borrowings
     * @param id id of the client
     * @param forename forename of the client
     * @param surname surname of the client; can not be NULL
     * @param registerDate registration date of the client
     */
    public Client(long id, String forename, String surname, Date registerDate) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.registerDate = registerDate;
        this.myBorrowings = new ArrayList<>();
    }

    /**
     * Create new Client with all attributes
     * @param forename forename of the client; can not be NULL
     * @param surname surname of the client; can not be NULL
     * @param registerDate date of registration; can not be NULL
     * @throws NullPointerException if either of forename, surname, registerDate is null
     */
    public Client(String forename, String surname, Date registerDate) {
        this.forename = Objects.requireNonNull(forename);
        this.surname = Objects.requireNonNull(surname);
        this.registerDate = Objects.requireNonNull(registerDate);
        myBorrowings = new ArrayList<>();
    }

    /**
     * Create new Client with all attributes
     * @param forename forename of the client; can not be NULL
     * @param surname surname of the client; can not be NULL
     * @throws NullPointerException if either of forename, surname, registerDate is null
     */
    public Client(String forename, String surname) {
        this.forename = Objects.requireNonNull(forename);
        this.surname = Objects.requireNonNull(surname);
        this.registerDate = null;
        myBorrowings = new ArrayList<>();
    }

    /**
     * Create new empty object of client
     */
    public Client() {
        this.id = 0;
        this.forename = "";
        this.surname = "";
        this.registerDate = new Date();
        myBorrowings = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Collection<Borrowing> getMyBorrowings() {
        return myBorrowings;
    }

    public void setMyBorrowings(Collection<Borrowing> myBorrowings) {
        this.myBorrowings = myBorrowings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return id == client.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
