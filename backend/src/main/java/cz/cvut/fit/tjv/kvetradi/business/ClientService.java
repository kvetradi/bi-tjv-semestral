package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.domain.Client;

public interface ClientService extends AbstractCrudService<Client, Long> {
    Long count();
}
