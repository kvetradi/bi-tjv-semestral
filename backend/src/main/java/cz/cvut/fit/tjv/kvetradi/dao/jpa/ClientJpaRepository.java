package cz.cvut.fit.tjv.kvetradi.dao.jpa;

import cz.cvut.fit.tjv.kvetradi.domain.Client;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Primary
public interface ClientJpaRepository extends JpaRepository<Client, Long> {
    Collection<Client> findAllByForename(@Param("forename") String forename);
}
