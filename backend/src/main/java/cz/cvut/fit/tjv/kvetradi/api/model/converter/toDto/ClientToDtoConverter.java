package cz.cvut.fit.tjv.kvetradi.api.model.converter.toDto;

import cz.cvut.fit.tjv.kvetradi.api.model.client.ClientDto;
import cz.cvut.fit.tjv.kvetradi.domain.Client;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ClientToDtoConverter implements Function<Client, ClientDto> {
    @Override
    public ClientDto apply(Client client) {
        ClientDto dto = new ClientDto();
        dto.setForename(client.getForename());
        dto.setSurname(client.getSurname());
        dto.setId(client.getId());
        return dto;
    }
}
