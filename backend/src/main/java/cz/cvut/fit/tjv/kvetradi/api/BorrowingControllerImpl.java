package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.borrowing.BorrowingDto;
import cz.cvut.fit.tjv.kvetradi.business.*;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.function.Function;

@RestController
@RequestMapping("/borrowings")
@Tag(name = "Borrowings", description = "Borrowing REST API endpoint")
public class BorrowingControllerImpl extends AbstractControllerImpl<Borrowing, BorrowingDto, Long> implements BorrowingController {

    private final BorrowingService borrowingService;

    public BorrowingControllerImpl(BorrowingService borrowingService, Function<Borrowing, BorrowingDto> toDtoConverter, ClientService clientService, BookService bookServiceImpl) {
        super(toDtoConverter, dto -> new Borrowing(
                dto.getId(),
                dto.getBorrowDate(),
                dto.getReturnDate(),
                dto.getNote(),
                dto.getFee(),
                clientService.readByID(dto.getClient().getId()).orElseThrow(() -> new IllegalArgumentException("Client with this ID does not exist")),
                dto.getBorrowed().stream().map(book -> bookServiceImpl.readByID(book.getId()).orElseThrow(() -> new IllegalArgumentException("Book with this ID does not exist"))).toList()
        ));
        this.borrowingService = borrowingService;
    }

    @Override
    protected AbstractCrudService<Borrowing, Long> getService() {
        return borrowingService;
    }

    @GetMapping("/withNotes")
    @ApiResponses(
            @ApiResponse(responseCode = "200")
    )
    public Collection<BorrowingDto> onlyWithNotes() {
        return borrowingService.findWithNotes().stream().map(borrowing -> toDtoConverter.apply(borrowing)).toList();
    }

    @Override
    @GetMapping("/notReturned")
    public Collection<BorrowingDto> notReturned() {
        return borrowingService.findNotReturned().stream().map(toDtoConverter).toList();
    }

    @Override
    @GetMapping("/returned")
    public Collection<BorrowingDto> returned() {
        return borrowingService.findReturned().stream().map(toDtoConverter).toList();
    }

    @Override
    @GetMapping("/count")
    public Long count() {
        return borrowingService.count();
    }

    @Override
    @GetMapping("/book/{id}")
    public Collection<BorrowingDto> findBorrowingsForBook(@PathVariable Long id) {
        try {
            return borrowingService.findBorrowingsForBook(id).stream().map(toDtoConverter).toList();
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
