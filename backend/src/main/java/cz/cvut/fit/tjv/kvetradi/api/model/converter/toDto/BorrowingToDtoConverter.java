package cz.cvut.fit.tjv.kvetradi.api.model.converter.toDto;

import cz.cvut.fit.tjv.kvetradi.api.model.borrowing.BorrowingDto;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BorrowingToDtoConverter implements Function<Borrowing, BorrowingDto> {
    BookToDtoConverter bookToDto;
    ClientToDtoConverter clientToDto;

    public BorrowingToDtoConverter(BookToDtoConverter bookToDto, ClientToDtoConverter clientToDto) {
        this.bookToDto = bookToDto;
        this.clientToDto = clientToDto;
    }

    @Override
    public BorrowingDto apply(Borrowing borrowing) {
        BorrowingDto dto = new BorrowingDto();
        dto.setId(borrowing.getId());
        dto.setFee(borrowing.getFee());
        dto.setNote(borrowing.getNote());
        dto.setBorrowDate(borrowing.getBorrowDate());
        dto.setReturnDate(borrowing.getReturnDate());
        dto.setBorrowed(borrowing.getBorrowed().stream().map(bookToDto).toList());
        dto.setClient(clientToDto.apply(borrowing.getClient()));
        return dto;
    }
}
