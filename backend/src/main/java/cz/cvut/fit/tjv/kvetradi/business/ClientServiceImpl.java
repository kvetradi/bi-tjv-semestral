package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.ClientJpaRepository;
import cz.cvut.fit.tjv.kvetradi.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
public class ClientServiceImpl extends AbstractCrudServiceImpl<Client, Long> implements ClientService {
    private final ClientJpaRepository clientJpaRepository;
    public ClientServiceImpl(ClientJpaRepository repository) {
        this.clientJpaRepository = repository;
    }

    @Override
    protected JpaRepository<Client, Long> getRepository() {
        return clientJpaRepository;
    }

    @Override
    public Long count() {
        return getRepository().count();
    }
}
