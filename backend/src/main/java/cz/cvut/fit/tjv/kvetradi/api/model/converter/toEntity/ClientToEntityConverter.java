package cz.cvut.fit.tjv.kvetradi.api.model.converter.toEntity;

import cz.cvut.fit.tjv.kvetradi.api.model.client.ClientDto;
import cz.cvut.fit.tjv.kvetradi.domain.Client;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ClientToEntityConverter implements Function<ClientDto, Client> {
    @Override
    public Client apply(ClientDto clientDto) {
        return new Client(
                clientDto.getForename(),
                clientDto.getSurname()
        );
    }
}
