package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.book.BookDto;
import cz.cvut.fit.tjv.kvetradi.business.AbstractCrudService;
import cz.cvut.fit.tjv.kvetradi.business.BookService;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.function.Function;

@RestController
@RequestMapping("/books")
@Tag(name = "Books", description = "Book REST API endpoint")
public class BookControllerImpl extends AbstractControllerImpl<Book, BookDto, Long> implements BookController {
    private final BookService bookService;

    public BookControllerImpl(BookService service, Function<Book, BookDto> toDtoConverter, Function<BookDto, Book> toEntityConverter) {
        super(toDtoConverter, toEntityConverter);
        this.bookService = service;
    }

    @Override
    protected AbstractCrudService<Book, Long> getService() {
        return bookService;
    }

    @GetMapping("/free")
    public Collection<BookDto> readAllFree() {
        return bookService.freeBooks().stream().map(toDtoConverter).toList();
    }

    @Override
    @GetMapping("/count")
    public Long count() {
        return bookService.count();
    }
}
