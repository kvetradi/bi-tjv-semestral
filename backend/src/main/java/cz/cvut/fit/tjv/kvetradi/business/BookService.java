package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.domain.Book;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;

import java.util.Collection;

public interface BookService extends AbstractCrudService<Book, Long> {
    Collection<Book> freeBooks();

    Long count();
}
