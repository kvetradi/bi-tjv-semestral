package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.BookJpaRepository;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class BookServiceImpl extends AbstractCrudServiceImpl<Book, Long> implements BookService {
    private final BorrowingServiceImpl borrowingService;
    private final BookJpaRepository bookJpaRepository;

    @Override
    protected JpaRepository<Book, Long> getRepository() {
        return bookJpaRepository;
    }

    public BookServiceImpl(BookJpaRepository bookJpaRepository, BorrowingServiceImpl borrowingService) {
        this.borrowingService = borrowingService;
        this.bookJpaRepository = bookJpaRepository;
    }

    public Collection<Book> freeBooks() {
        Collection<Book> allBooks = getRepository().findAll();
        Collection<Long> unreturnedBook = borrowingService.findNotReturned().stream().flatMap(borrowing -> borrowing.getBorrowed().stream().map(Book::getId)).toList();
        return allBooks.stream().filter(book -> !unreturnedBook.contains(book.getId())).toList();
    }

    @Override
    public Long count() {
        return getRepository().count();
    }
}
