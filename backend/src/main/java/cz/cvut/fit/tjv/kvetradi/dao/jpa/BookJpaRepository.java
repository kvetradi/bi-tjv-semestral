package cz.cvut.fit.tjv.kvetradi.dao.jpa;

import cz.cvut.fit.tjv.kvetradi.domain.Book;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Primary
public interface BookJpaRepository extends JpaRepository<Book, Long> {
    Collection<Book> findAllByAuthorName(@Param("author") String author);
}
