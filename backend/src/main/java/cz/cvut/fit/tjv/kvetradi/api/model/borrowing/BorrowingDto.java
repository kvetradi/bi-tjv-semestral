package cz.cvut.fit.tjv.kvetradi.api.model.borrowing;

import cz.cvut.fit.tjv.kvetradi.api.model.client.ClientDto;
import cz.cvut.fit.tjv.kvetradi.api.model.book.BookDto;

import java.util.Collection;
import java.util.Date;

public class BorrowingDto {
    private long id;
    private Date borrowDate;
    private Date returnDate;
    private String note;
    private int fee;
    private Collection<BookDto> borrowed;
    private ClientDto client;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public Collection<BookDto> getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(Collection<BookDto> borrowed) {
        this.borrowed = borrowed;
    }

    public ClientDto getClient() {
        return client;
    }

    public void setClient(ClientDto client) {
        this.client = client;
    }
}
