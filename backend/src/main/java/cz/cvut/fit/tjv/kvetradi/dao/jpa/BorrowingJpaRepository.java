package cz.cvut.fit.tjv.kvetradi.dao.jpa;

import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;

@Repository
@Primary
public interface BorrowingJpaRepository extends JpaRepository<Borrowing, Long> {
    Collection<Borrowing> findAllByBorrowDateAfter(@Param("date") Date date);

    Collection<Borrowing> findAllByReturnDateNull();

    Collection<Borrowing> findAllByReturnDateNotNull();

    @Query("SELECT b FROM Borrowing b WHERE b.note IS NOT NULL AND b.note != ''")
    Collection<Borrowing> findAllByNoteIsNotNullAndNoteIsNotEmpty();

    Collection<Borrowing> findDistinctByBorrowedId(@Param("id") Long id);
}
