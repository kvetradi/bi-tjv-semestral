package cz.cvut.fit.tjv.kvetradi.api.model.converter.toEntity;

import cz.cvut.fit.tjv.kvetradi.api.model.book.BookDto;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BookToEntityConverter implements Function<BookDto, Book> {
    @Override
    public Book apply(BookDto bookDto) {
        return new Book(
                bookDto.getName(),
                bookDto.getNumberOfPages(),
                bookDto.getReleaseDate(),
                bookDto.getAuthorName()
        );
    }
}
