package cz.cvut.fit.tjv.kvetradi.api.model.converter.toDto;

import cz.cvut.fit.tjv.kvetradi.api.model.book.BookDto;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class BookToDtoConverter implements Function<Book, BookDto> {
    @Override
    public BookDto apply(Book book) {
        BookDto bookDto = new BookDto();
        bookDto.setId(book.getId());
        bookDto.setName(book.getName());
        bookDto.setAuthorName(book.getAuthorName());
        bookDto.setNumberOfPages(book.getNumberOfPages());
        bookDto.setReleaseDate(book.getReleaseDate());
        return bookDto;
    }
}
