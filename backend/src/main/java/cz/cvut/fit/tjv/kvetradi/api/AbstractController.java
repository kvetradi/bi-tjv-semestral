package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.domain.DomainEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collection;

public interface AbstractController<E extends DomainEntity<ID>, D, ID> {
    D create(@RequestBody D t);
    Collection<D> readAll();
    ResponseEntity<D> readOne(@PathVariable ID id);
    ResponseEntity<D> update(@RequestBody D entity, @PathVariable ID id);
    void delete(@PathVariable ID id);
}
