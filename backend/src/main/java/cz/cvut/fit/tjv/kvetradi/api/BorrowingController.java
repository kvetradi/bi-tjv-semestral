package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.borrowing.BorrowingDto;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;

import java.util.Collection;

public interface BorrowingController extends AbstractController<Borrowing, BorrowingDto, Long> {
    /**
     * Find borrowing only containing notes
     * @return Collection of Borrowings containing notes
     */
    Collection<BorrowingDto> onlyWithNotes();

    /**
     * Find all not returned borrowings
     * @return Collection of not returned borrowings
     */
    Collection<BorrowingDto> notReturned();

    /**
     * Find all returned borrowings
     * @return Collection of returned borrowings
     */
    Collection<BorrowingDto> returned();

    /**
     * Find how many borrowings we have in our system.
     * This counts returned and not returned alike.
     * @return number of all borrowings our system did.
     */
    Long count();

    /**
     * Find associate borrowings for a specified book ID.
     * @param id id of the books we want to find the borrowings for.
     * @return Collection of all the borrowings this books has.
     */
    Collection<BorrowingDto> findBorrowingsForBook(Long id);
}
