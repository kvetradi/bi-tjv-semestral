package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.api.model.client.ClientDto;
import cz.cvut.fit.tjv.kvetradi.domain.Client;

public interface ClientController extends AbstractController<Client, ClientDto, Long> {
    /**
     * Return the number of active clients of our system.
     * @return count of all active clients
     */
    Long count();
}
