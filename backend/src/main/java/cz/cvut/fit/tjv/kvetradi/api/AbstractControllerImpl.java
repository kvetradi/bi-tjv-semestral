package cz.cvut.fit.tjv.kvetradi.api;

import cz.cvut.fit.tjv.kvetradi.business.AbstractCrudService;
import cz.cvut.fit.tjv.kvetradi.business.AbstractCrudServiceImpl;
import cz.cvut.fit.tjv.kvetradi.business.EntityStateException;
import cz.cvut.fit.tjv.kvetradi.domain.DomainEntity;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.StreamSupport;

public abstract class AbstractControllerImpl<E extends DomainEntity<ID>, D, ID> implements AbstractController<E, D, ID> {
    protected Function<E, D> toDtoConverter;
    protected Function<D, E> toEntityConverter;

    abstract protected AbstractCrudService<E, ID> getService();

    public AbstractControllerImpl(Function<E, D> toDtoConverter, Function<D, E> toEntityConverter) {
        this.toDtoConverter = toDtoConverter;
        this.toEntityConverter = toEntityConverter;
    }

    @PostMapping
    @ResponseBody
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
    })
    public D create(@RequestBody D t) {
        try {
            return toDtoConverter.apply(getService().create(toEntityConverter.apply(t)));
        } catch(EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @GetMapping
    @ResponseBody
    public Collection<D> readAll() {
        // we want to apply toDtoConverter to every item from service.readAll. But collection doesn't have .stream()
        // so we get it from StreamSupport object.
        return StreamSupport.stream(getService().readAll().spliterator(), false).map(toDtoConverter).toList();
    }

    @GetMapping("/{id}")
    public ResponseEntity<D> readOne(@PathVariable ID id) {
//        return ResponseEntity.of(service.readByID(id));
        var entity = getService().readByID(id);
        return entity.map(e -> ResponseEntity.ok(toDtoConverter.apply(e))).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<D> update(@RequestBody D entity, @PathVariable ID id) {
        try {
            var result = getService().update(id, toEntityConverter.apply(entity));
            return ResponseEntity.ok(toDtoConverter.apply(result));
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable ID id) {
        getService().deleteById(id);
    }
}
