package cz.cvut.fit.tjv.kvetradi.domain;

public interface DomainEntity<T> {
    T getId();
}
