package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.domain.DomainEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public abstract class AbstractCrudServiceImpl<E extends DomainEntity<K>, K> implements AbstractCrudService<E, K> {
    abstract protected JpaRepository<E, K> getRepository();

    /**
     * Create new entity in the repository
     * @param entity entity to create
     * @return created entity
     * @throws EntityStateException if entity is already present in the repository
     */
    public E create(E entity) {
        K id = entity.getId();
        if (id != null && getRepository().existsById(id))
            throw new EntityStateException();
        return getRepository().save(entity);
    }

    /**
     * Update given entity in repository.
     * @param entity entity to update
     * @return new updated entity
     * @throws EntityStateException if entity does not exist in repository
     */
    public E update(E entity) {
        if (entity.getId() == null || !getRepository().existsById(entity.getId()))
            throw new EntityStateException();
        return getRepository().save(entity);
    }

    public E update(K id, E e) {
        if (!getRepository().existsById(id))
            throw new EntityStateException();

        return getRepository().save(e);
    }

    /**
     * Get entity by with given id if it exists
     * @param id ID of the entity we want
     * @return Optional of Entity with value of found entity, if not found null is present
     */
    public Optional<E> readByID(K id) {
        return getRepository().findById(id);
    }

    /**
     * Get all entities in the repository
     * @return Iterable of all found entities in the repository
     */
    public Iterable<E> readAll() {
        return getRepository().findAll();
    }

    /**
     * Delete entity with given id from repository
     * @param key id of the entity we want to delete
     */
    public void deleteById(K key) {
        getRepository().deleteById(key);
    }

    /**
     * Delete provided entity if it exists in repository
     * @param entity entity to delete
     */
    public void delete(E entity) {
        getRepository().delete(entity);
    }
}
