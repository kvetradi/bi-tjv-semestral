package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;

import java.util.Collection;

public interface BorrowingService extends AbstractCrudService<Borrowing, Long>{
    Collection<Borrowing> findReturned();
    Collection<Borrowing> findNotReturned();
    Collection<Borrowing> findWithNotes();
    Long count();
    /**
     * Find all borrowings for a book with given ID.
     * @param id id of the books we want the borrowings for.
     * @return Collection of all the borrowings this book has.
     * @throws EntityStateException if book with the given ID does not exists.
     */
    Collection<Borrowing> findBorrowingsForBook(Long id);
}
