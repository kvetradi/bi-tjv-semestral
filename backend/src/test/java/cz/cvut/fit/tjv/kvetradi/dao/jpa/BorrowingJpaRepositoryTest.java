package cz.cvut.fit.tjv.kvetradi.dao.jpa;

import cz.cvut.fit.tjv.kvetradi.domain.Book;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import cz.cvut.fit.tjv.kvetradi.domain.Client;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@DataJpaTest
public class BorrowingJpaRepositoryTest {
    @Autowired
    private BorrowingJpaRepository borrowingJpaRepository;
    @Autowired
    private ClientJpaRepository clientJpaRepository;
    @Autowired
    private BookJpaRepository bookJpaRepository;

    @BeforeEach
    void setUp() {
        borrowingJpaRepository.deleteAll();
        clientJpaRepository.deleteAll();
        bookJpaRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        borrowingJpaRepository.deleteAll();
        clientJpaRepository.deleteAll();
        bookJpaRepository.deleteAll();
    }

    private Book addBook() {
        Book book = new Book("bookXY", 0, new Date(), "author");
        return bookJpaRepository.save(book);
    }

    private Client addClient() {
        Client c = new Client(1L, "name1", "surname2", new Date());
        return clientJpaRepository.save(c);
    }

    @Test
    public void shouldFindOneWithNotes() {
        Book book1 = addBook();

        Borrowing t1 = new Borrowing(1L, new Date(), new Date(), "With note", 0, null, List.of(book1));
        Borrowing t2 = new Borrowing(2L, new Date(), new Date(), "", 0, null, List.of());

        var b1 = borrowingJpaRepository.save(t1);
        var b2 = borrowingJpaRepository.save(t2);

        var result = borrowingJpaRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty();
        Assertions.assertEquals(List.of(b1), result);
    }

    @Test
    public void shouldFindMoreWithNotes() {
        Book book1 = addBook();

        Borrowing t1 = new Borrowing(1L, new Date(), new Date(), "With note", 0, null, List.of(book1));
        Borrowing t2 = new Borrowing(2L, new Date(), new Date(), "Another note", 0, null, List.of(book1));

        var b1 = borrowingJpaRepository.save(t1);
        var b2 = borrowingJpaRepository.save(t2);

        var result = borrowingJpaRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty();
        Assertions.assertEquals(List.of(b1, b2), result);
    }

    @Test
    public void shouldFindNoneWithNotes() {
        Book book1 = addBook();
        Borrowing t1 = new Borrowing(1L, new Date(), new Date(), "", 0, null, List.of(book1));
        Borrowing t2 = new Borrowing(2L, new Date(), new Date(), "", 0, null, List.of());

        borrowingJpaRepository.save(t1);
        borrowingJpaRepository.save(t2);

        var result = borrowingJpaRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty();
        Assertions.assertEquals(List.of(), result);
    }

    @Test
    public void shouldFindNothingOnEmptyRepository() {
        var result = borrowingJpaRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty();
        Assertions.assertEquals(List.of(), result);
    }
}
