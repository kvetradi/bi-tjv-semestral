package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.ClientJpaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class ClientServiceImplTest {
    @Autowired
    private ClientServiceImpl clientService;
    @MockBean
    private ClientJpaRepository clientRepository;

    @Test
    public void shouldReturnCount() {
        var count = clientService.count();
        Assertions.assertEquals(0, count, "There should be 0 clients");
    }

    @Test
    public void shouldReturnOneClientCount() {
        Mockito.when(clientRepository.count()).thenReturn(1L);

        var count = clientService.count();
        Assertions.assertEquals(1, count, "There should be 1 client");
    }
}
