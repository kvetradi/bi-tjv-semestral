package cz.cvut.fit.tjv.kvetradi.business;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import java.util.Date;
import java.util.List;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.BookJpaRepository;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import cz.cvut.fit.tjv.kvetradi.domain.Client;

@SpringBootTest
public class BookServiceTest {
    @Autowired
    private BookServiceImpl bookServiceImpl;
    @MockBean
    private BookJpaRepository bookRepository;
    @MockBean
    private BorrowingServiceImpl borrowingService;

    private void addBook(Book book) {
        Mockito.when(bookRepository.save(book)).thenReturn(book);
        Mockito.when(bookRepository.findById(book.getId())).thenReturn(java.util.Optional.of(book));
        Mockito.when(bookRepository.existsById(book.getId())).thenReturn(true);
    }

    private void addBooks(List<Book> books) {
        Mockito.when(bookRepository.saveAll(books)).thenReturn(books);
        Mockito.when(bookRepository.findAllById(books.stream().map(Book::getId).toList())).thenReturn(books);
        Mockito.when(bookRepository.findAll()).thenReturn(books);
        books.forEach(book -> {
            Mockito.when(bookRepository.findById(book.getId())).thenReturn(java.util.Optional.of(book));
            Mockito.when(bookRepository.existsById(book.getId())).thenReturn(true);
        });
    }

    private List<Book> addThreeBooks() {
        Book book1 = new Book(1L, "Book1", 10, new Date(), "Author1");
        Book book2 = new Book(2L, "Book2", 11, new Date(), "Author2");
        Book book3 = new Book(3L, "book3", 12, new Date(), "Author3");

        addBooks(List.of(book1, book2, book3));
        return List.of(book1, book2, book3);
    }

    @Test
    public void shouldReturnOnlyBooksNotBorrowed() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        var borrowedBooks = List.of(book2);
        var freeBooks = List.of(book1, book3);

        Borrowing borrowing = new Borrowing(1L, new Date(), null, "", 0, new Client(), borrowedBooks);
        var allBorrowings = List.of(borrowing);

        Mockito.when(borrowingService.findNotReturned()).thenReturn(allBorrowings);

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(freeBooks, result, "There should be 2 same books");
    }

    @Test
    public void shouldReturnEmptyFreeBooksWhenAllBorrowed() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        Borrowing borrowing = new Borrowing(1L, new Date(), null, "", 0, new Client(), List.of(book1, book2, book3));
        Mockito.when(borrowingService.findNotReturned()).thenReturn(List.of(borrowing));

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(List.of(), result, "There should be no books");
    }

    @Test
    public void shouldReturnEmptyFreeBooksWhenNoBooks() {
        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(List.of(), result, "There should be no books");
    }

    @Test
    public void shouldReturnAllBooksIfNoneBorrowed() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        var freeBooks = List.of(book1, book2, book3);

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(freeBooks, result, "There should be 3 free books");
    }

    @Test
    public void shouldReturnAllBooksIfAllReturned() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        var freeBooks = List.of(book1, book2, book3);

        Borrowing borrowing = new Borrowing(1L, new Date(), new Date(), "", 0, new Client(), List.of(book1, book2, book3));
        Mockito.when(borrowingService.findReturned()).thenReturn(List.of(borrowing));

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(freeBooks, result, "There should be 3 free books");
    }

    @Test
    public void shouldReturnNoBooksIfAllReturnedAndBorrowed() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        Borrowing returnedBorrowing = new Borrowing(1L, new Date(), new Date(), "", 0, new Client(), List.of(book1, book2, book3));
        Borrowing notReturnedBorrowing = new Borrowing(1L, new Date(), null, "", 0, new Client(), List.of(book1, book2, book3));
        Mockito.when(borrowingService.findReturned()).thenReturn(List.of(returnedBorrowing));
        Mockito.when(borrowingService.findNotReturned()).thenReturn(List.of(notReturnedBorrowing));

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(List.of(), result, "There should be no books");
    }

    @Test
    public void shouldReturnSomeBooksIfAllReturnedAndSomeBorrowed() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        Borrowing returnedBorrowing = new Borrowing(1L, new Date(), new Date(), "", 0, new Client(), List.of(book1, book2, book3));
        Borrowing notReturnedBorrowing = new Borrowing(1L, new Date(), null, "", 0, new Client(), List.of(book3));
        Mockito.when(borrowingService.findReturned()).thenReturn(List.of(returnedBorrowing));
        Mockito.when(borrowingService.findNotReturned()).thenReturn(List.of(notReturnedBorrowing));

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(List.of(book1, book2), result, "There should be no books");
    }

    @Test
    public void shouldReturnNoBooksIfBorrowedInMultipleBorrowings() {
        var books = addThreeBooks();
        var book1 = books.get(0);
        var book2 = books.get(1);
        var book3 = books.get(2);

        Borrowing bor1 = new Borrowing(1L, new Date(), null, "", 0, new Client(), List.of(book1));
        Borrowing bor2 = new Borrowing(1L, new Date(), null, "", 0, new Client(), List.of(book2, book3));
        Mockito.when(borrowingService.findNotReturned()).thenReturn(List.of(bor1, bor2));

        var result = bookServiceImpl.freeBooks();
        Assertions.assertEquals(List.of(), result, "There should be no books");
    }

    @Test
    public void shouldReturnCount() {
        Mockito.when(bookRepository.count()).thenReturn(5L);
        var result = bookServiceImpl.count();

        Assertions.assertEquals(5L, result, "There should be 5 books");
    }
}
