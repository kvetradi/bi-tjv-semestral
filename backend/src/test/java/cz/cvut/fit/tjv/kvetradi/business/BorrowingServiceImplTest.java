package cz.cvut.fit.tjv.kvetradi.business;

import cz.cvut.fit.tjv.kvetradi.dao.jpa.BookJpaRepository;
import cz.cvut.fit.tjv.kvetradi.dao.jpa.BorrowingJpaRepository;
import cz.cvut.fit.tjv.kvetradi.domain.Book;
import cz.cvut.fit.tjv.kvetradi.domain.Borrowing;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class BorrowingServiceImplTest {
    @Autowired
    private BorrowingServiceImpl borrowingService;
    @MockBean
    private BorrowingJpaRepository borrowingRepository;
    @MockBean
    private BookJpaRepository bookRepository;

    @AfterEach
    void tearDown() {
        borrowingRepository.deleteAll();
        bookRepository.deleteAll();
    }

    @BeforeEach
    void setUp() {
        borrowingRepository.deleteAll();
        bookRepository.deleteAll();
    }

    private void insertBook(Book book) {
        Mockito.when(bookRepository.findById(book.getId())).thenReturn(Optional.of(book));
        Mockito.when(bookRepository.existsById(book.getId())).thenReturn(true);
    }

    private List<Book> insertTwoBooks() {
        Book book1 = new Book(1L, "Book1", 10, new Date(), "Author1");
        Book book2 = new Book(2L, "Book2", 11, new Date(), "Author2");

        insertBook(book1);
        insertBook(book2);

        return List.of(book1, book2);
    }

    @Test
    public void shouldReturnOneReturnedBorrowing() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(books.get(0)));
        Borrowing b2 = new Borrowing(2L, new Date(), new Date(), "", 0, null, List.of(books.get(1)));

        Mockito.when(borrowingRepository.findAllByReturnDateNotNull()).thenReturn(List.of(b2));
        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of(b1));

        var result = borrowingService.findReturned();
        Assertions.assertEquals(1, result.size(), "There should be 1 returned borrowing");
        Assertions.assertEquals(List.of(b2), result, "There should be 1 returned borrowing");
    }

    @Test
    public void shouldReturnMoreReturnedBorrowings() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(books.get(0)));
        Borrowing b2 = new Borrowing(2L, new Date(), new Date(), "", 0, null, List.of(books.get(0)));
        Borrowing b3 = new Borrowing(3L, new Date(), new Date(), "", 0, null, List.of(books.get(1)));

        Mockito.when(borrowingRepository.findAllByReturnDateNotNull()).thenReturn(List.of(b2, b3));
        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of(b1));

        var result = borrowingService.findReturned();
        Assertions.assertEquals(2, result.size(), "There should be 1 returned borrowing");
        Assertions.assertEquals(List.of(b2, b3), result, "There should be 1 returned borrowing");
    }

    @Test
    public void shouldReturnEmptyReturnedBorrowingsWhenNoneReturned() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(books.get(0)));

        Mockito.when(borrowingRepository.findAllByReturnDateNotNull()).thenReturn(List.of());
        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of(b1));

        var result = borrowingService.findReturned();
        Assertions.assertEquals(List.of(), result, "There should be 0 returned borrowing");
    }

    @Test
    public void shouldReturnEmptyReturnedBorrowingsWhenNoBorrowings() {
        Mockito.when(borrowingRepository.findAllByReturnDateNotNull()).thenReturn(List.of());
        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of());

        var result = borrowingService.findReturned();
        Assertions.assertEquals(List.of(), result, "There should be 0 returned borrowing");
    }

    @Test
    public void shouldReturnOneNotReturnedBorrowing() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(books.get(0)));

        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of(b1));

        var result = borrowingService.findNotReturned();
        Assertions.assertEquals(List.of(b1), result, "There should be 1 returned borrowing");
    }

    @Test
    public void shouldReturnMoreNotReturnedBorrowing() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(books.get(0)));
        Borrowing b2 = new Borrowing(2L, new Date(), null, "", 0, null, books);

        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of(b1, b2));

        var result = borrowingService.findNotReturned();
        Assertions.assertEquals(List.of(b1, b2), result, "There should be 2 returned borrowings");
    }

    @Test
    public void shouldReturnEmptyNotReturnedBorrowing() {
        Mockito.when(borrowingRepository.findAllByReturnDateNull()).thenReturn(List.of());

        var result = borrowingService.findNotReturned();
        Assertions.assertEquals(List.of(), result, "There should be 0 returned borrowings");
    }

    @Test
    public void shouldReturnMoreBorrowingsForBook() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, List.of(book1));
        Borrowing b2 = new Borrowing(2L, new Date(), new Date(), "", 0, null, books);

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of(b1, b2));
        var result = borrowingService.findBorrowingsForBook(book1.getId());

        Assertions.assertEquals(List.of(b1, b2), result);
    }

    @Test
    public void shouldReturnNoBorrowingsForBook() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of());
        var result = borrowingService.findBorrowingsForBook(book1.getId());

        Assertions.assertEquals(List.of(), result);
    }

    @Test
    public void shouldReturnOneReturnedBorrowingForBook() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Borrowing b1 = new Borrowing(1L, new Date(), new Date(), "", 0, null, books);

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of(b1));
        var result = borrowingService.findBorrowingsForBook(book1.getId());

        Assertions.assertEquals(List.of(b1), result);
    }

    @Test
    public void shouldReturnOneNotReturnedBorrowingForBook() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, books);

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of(b1));
        var result = borrowingService.findBorrowingsForBook(book1.getId());

        Assertions.assertEquals(List.of(b1), result);
    }

    @Test
    public void shouldThrowOnInvalidId() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Borrowing b1 = new Borrowing(1L, new Date(), null, "", 0, null, books);

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of(b1));
        Assertions.assertThrows(EntityStateException.class, () -> borrowingService.findBorrowingsForBook(-1L));
    }

    @Test
    public void shouldReturnOneBorrowingForOnlyBookInBorrowing() {
        var books = insertTwoBooks();

        var book1 = books.get(0);

        Borrowing b1 = new Borrowing(1L, new Date(), new Date(), "", 0, null, List.of(book1));

        Mockito.when(borrowingRepository.findDistinctByBorrowedId(book1.getId())).thenReturn(List.of(b1));
        var result = borrowingService.findBorrowingsForBook(book1.getId());

        Assertions.assertEquals(List.of(b1), result);
    }

    @Test
    public void shouldReturnOneBorrowingWithNotes() {
        var books = insertTwoBooks();

        Borrowing b1 = new Borrowing(1L, new Date(), new Date(), "note", 0, null, books);

        Mockito.when(borrowingRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty()).thenReturn(List.of(b1));
        var result = borrowingService.findWithNotes();

        Assertions.assertEquals(List.of(b1), result);
    }

    @Test
    public void shouldReturnNoBorrowingWithNotes() {
        Mockito.when(borrowingRepository.findAllByNoteIsNotNullAndNoteIsNotEmpty()).thenReturn(List.of());
        var result = borrowingService.findWithNotes();

        Assertions.assertEquals(List.of(), result);
    }

    @Test
    public void shouldReturnCount() {
        Mockito.when(borrowingRepository.count()).thenReturn(1L);
        Assertions.assertEquals(1L, borrowingService.count());
    }
}
