import { Stack } from "@mui/material";
import { FormContainer, TextFieldElement, useForm } from "react-hook-form-mui";
import { toast } from "react-toastify";

import { LoadingButton } from "@/components/button/LoadingButton";
import { Heading } from "@/components/typography/Heading";
import { useMutateClient } from "@/queryHooks/Client";

interface FormClient {
	forename: string;
	surname: string;
}

const emptyClient = {
	forename: "",
	surname: "",
};

const NewClient = () => {
	const { mutateAsync, isPending } = useMutateClient({
		options: {
			onSuccess: () => {
				toast.success("Client created");
				formContext.reset(emptyClient);
			},
			onError: () => {
				toast.error("Something went wrong");
			},
		},
	});
	const formContext = useForm<FormClient>({
		defaultValues: emptyClient,
	});

	const handleSubmit = (data: FormClient) => {
		void mutateAsync({ clientDto: data });
	};

	return (
		<Stack alignItems="center" gap={4}>
			<Heading>Register new Client</Heading>
			<FormContainer formContext={formContext} onSuccess={handleSubmit}>
				<Stack gap={4}>
					<TextFieldElement
						required
						label="First name"
						name="forename"
						placeholder="John"
						validation={{
							required: true,
							minLength: 2,
						}}
					/>
					<TextFieldElement
						required
						label="Last name"
						name="surname"
						placeholder="Lemon"
						validation={{
							required: true,
							minLength: 2,
						}}
					/>
					<LoadingButton isLoading={isPending} type="submit">
						Register
					</LoadingButton>
				</Stack>
			</FormContainer>
		</Stack>
	);
};

export default NewClient;
