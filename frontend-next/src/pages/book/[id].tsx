import { Stack } from "@mui/material";
import { useRouter } from "next/router";

import { BookDetail } from "@/components/BookDetail";
import { BookRentsList } from "@/components/BookRentsList";

const Book = () => {
	const router = useRouter();
	const { id } = router.query;

	if (id === undefined || isNaN(Number(id))) return <Stack>There is a problem with the URL</Stack>;

	return (
		<Stack gap={4}>
			<BookDetail id={Number(id)} />
			<BookRentsList bookId={Number(id)} />
		</Stack>
	);
};

export default Book;
