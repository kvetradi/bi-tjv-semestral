import { Stack } from "@mui/material";
import { DatePickerElement, FormContainer, TextFieldElement, useForm } from "react-hook-form-mui";
import { toast } from "react-toastify";

import { LoadingButton } from "@/components/button/LoadingButton";
import { Heading } from "@/components/typography/Heading";
import { useMutateBook } from "@/queryHooks/Book";

interface FormBook {
	name: string;
	numberOfPages: number;
	releaseDate?: string;
	authorName?: string;
}

const emptyBook = {
	name: "",
	numberOfPages: undefined,
	releaseDate: undefined,
	authorName: "",
};

const NewBook = () => {
	const { mutateAsync, isPending } = useMutateBook({
		options: {
			onSuccess: () => {
				toast.success("New book created");
				formContext.reset(emptyBook);
			},
			onError: () => {
				toast.error("Something went wrong");
			},
		},
	});
	const formContext = useForm<FormBook>({
		defaultValues: emptyBook,
	});

	const handleSubmit = (data: FormBook) => {
		void mutateAsync({
			bookDto: {
				...data,
				releaseDate: data.releaseDate == null ? undefined : new Date(data.releaseDate),
			},
		});
	};

	return (
		<Stack alignItems="center" gap={4}>
			<Heading>Register new Book</Heading>
			<FormContainer
				formContext={formContext}
				onSuccess={handleSubmit}
				FormProps={{
					"aria-autocomplete": "none",
				}}
			>
				<Stack gap={4}>
					<TextFieldElement
						required
						label="Book name"
						name="name"
						placeholder="The best book"
						validation={{
							required: true,
							minLength: 2,
						}}
					/>
					<TextFieldElement
						required
						label="number of pages"
						name="numberOfPages"
						type="number"
						placeholder="5"
						validation={{
							required: true,
							min: 1,
						}}
					/>
					<TextFieldElement label="Author name" name="authorName" placeholder="John Lemon" />
					<DatePickerElement isDate label="Release date" name="releaseDate" />
					<LoadingButton isLoading={isPending} color="primary" type="submit">
						Register
					</LoadingButton>
				</Stack>
			</FormContainer>
		</Stack>
	);
};

export default NewBook;
