import "react-toastify/dist/ReactToastify.css";

import type { AppProps } from "next/app";
import React from "react";
import { ToastContainer } from "react-toastify";

import { ApiProvider } from "@/context/ApiProvider";
import ColorModeProvider from "@/context/ColorMode";
import DateFnsProvider from "@/context/DateFsnProvider";
import { QueryProvider } from "@/context/QueryProvider";
import Theme from "@/context/Theme";
import Layout from "@/layout/Layout";

export default function App({ Component, pageProps }: AppProps) {
	return (
		<ApiProvider>
			<QueryProvider>
				<ColorModeProvider>
					<Theme>
						<DateFnsProvider>
							<Layout>
								<Component {...pageProps} />
								<ToastContainer />
							</Layout>
						</DateFnsProvider>
					</Theme>
				</ColorModeProvider>
			</QueryProvider>
		</ApiProvider>
	);
}
