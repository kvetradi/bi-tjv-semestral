import { Stack } from "@mui/material";

import { BookCard } from "@/components/cards/BookCard";
import { HeadingL } from "@/components/typography/Heading";
import { useBooks } from "@/queryHooks/Book";

export default function Books() {
	const { data, isLoading } = useBooks();

	return (
		<Stack alignItems="center">
			<HeadingL>Books</HeadingL>
			<Stack direction="row" gap={4} flexWrap="wrap" justifyContent="center">
				{!isLoading && data?.map((book) => <BookCard key={book.id} book={book} />)}
			</Stack>
		</Stack>
	);
}
