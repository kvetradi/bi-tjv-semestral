import { Stack } from "@mui/material";

import { NotReturnedTable } from "@/components/table/borrowing/NotReturnedTable";
import { HeadingL } from "@/components/typography/Heading";

export default function Unreturned() {
	return (
		<Stack alignItems="center">
			<HeadingL>Unreturned Borrowings</HeadingL>
			<NotReturnedTable />
		</Stack>
	);
}
