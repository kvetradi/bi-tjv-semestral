import { Stack } from "@mui/material";

import { AllBorrowingTable } from "@/components/table/borrowing/AllBorrowingTable";
import { HeadingL } from "@/components/typography/Heading";

export default function All() {
	return (
		<Stack alignItems="center">
			<HeadingL>All Borrowings</HeadingL>
			<AllBorrowingTable />
		</Stack>
	);
}
