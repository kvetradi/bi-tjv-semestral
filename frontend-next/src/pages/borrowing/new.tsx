import { Skeleton, Stack } from "@mui/material";
import {
	AutocompleteElement,
	DatePickerElement,
	FormContainer,
	TextFieldElement,
	useForm,
} from "react-hook-form-mui";
import { toast } from "react-toastify";

import { LoadingButton } from "@/components/button/LoadingButton";
import { Heading } from "@/components/typography/Heading";
import { useBooks } from "@/queryHooks/Book";
import { useCreateMutateBorrowing } from "@/queryHooks/Borrowing";
import { useClients } from "@/queryHooks/Client";

import { type BookDto, type ClientDto } from "../../../models";

interface IOptionBook {
	id: number;
	value: BookDto;
	label: string;
}

interface IFormInterface {
	client: ClientDto;
	books: IOptionBook[];
	borrowDate: Date;
	returnDate: Date;
	note: string;
	fee: number;
}

const emptyBorrowing = {
	client: undefined,
	books: [],
	borrowDate: undefined,
	returnDate: undefined,
	note: "",
	fee: 0,
};

const NewBorrowing = () => {
	const { data: booksData, isLoading: booksLoading, isSuccess: booksSuccess } = useBooks();
	const { data: clientsData, isLoading: clientsLoading, isSuccess: clientsSuccess } = useClients();
	const { mutateAsync, isPending } = useCreateMutateBorrowing({
		options: {
			onSuccess: () => {
				toast.success("new borrowing created");
				formContext.reset(emptyBorrowing);
			},
			onError: () => {
				toast.error("Something went wrong");
			},
		},
	});

	const formContext = useForm<IFormInterface>({
		defaultValues: emptyBorrowing,
	});

	const handleSubmit = (data: IFormInterface) => {
		void mutateAsync({
			borrowingDto: {
				...data,
				returnDate: data.returnDate ?? undefined,
				borrowDate: data.borrowDate ?? undefined,
				borrowed: data.books.map((book: BookDto) => ({
					id: book.id,
					name: "",
					author: "",
				})),
			},
		});
	};

	if (booksLoading || clientsLoading) {
		return (
			<Stack>
				<Skeleton width="100%" height="50vh"></Skeleton>
			</Stack>
		);
	}

	if (!booksSuccess || !clientsSuccess) {
		return (
			<Stack>
				<Heading>Problem loading data</Heading>
			</Stack>
		);
	}

	return (
		<Stack alignItems="center">
			<Heading>New borrowing</Heading>
			<FormContainer
				onSuccess={handleSubmit}
				FormProps={{
					"aria-autocomplete": "none",
				}}
				formContext={formContext}
			>
				<Stack gap={4}>
					<AutocompleteElement
						name="client"
						label="Client who is borrowing"
						options={clientsData.map((user) => ({
							id: user.id,
							label: `${user.id} - ${user.forename} ${user.surname}`,
							value: user,
						}))}
						rules={{
							required: true,
						}}
					/>
					<AutocompleteElement
						multiple
						name="books"
						label="books which is he borrowing (multiselect)"
						options={booksData.map((book) => ({
							id: book.id,
							label: `${book.name} | by ${book.authorName}`,
							value: book,
						}))}
						rules={{
							required: true,
						}}
					/>
					<Stack direction="row" justifyContent="space-between" gap={4}>
						<DatePickerElement
							name="borrowDate"
							label="from"
							validation={{
								required: true,
							}}
						/>
						<DatePickerElement name="returnDate" label="to" />
					</Stack>
					<TextFieldElement name="note" label="Note" />
					<TextFieldElement
						required
						label="fee"
						name="fee"
						type="number"
						placeholder="0"
						validation={{
							required: true,
							min: 0,
						}}
					/>
					<LoadingButton isLoading={isPending} type="submit">
						Submit
					</LoadingButton>
				</Stack>
			</FormContainer>
		</Stack>
	);
};

export default NewBorrowing;
