import { Stack } from "@mui/material";

import { Statistics } from "@/components/Statistics";
import { Heading } from "@/components/typography/Heading";

export default function Home() {
	return (
		<Stack alignItems="center" gap={4}>
			<Heading>Welcome to book library</Heading>
			<Statistics />
		</Stack>
	);
}
