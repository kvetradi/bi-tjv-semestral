import { Container } from "@mui/material";
import Box from "@mui/material/Box";
import Head from "next/head";
import { type ReactNode } from "react";

import Header from "@/components/Header";

const Layout = ({ children }: { children: ReactNode }): JSX.Element => {
	return (
		<Box>
			<Head>
				<title>Výpůjčovna knih</title>
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<Header />
			<main>
				<Container sx={{ my: 4 }}>{children}</Container>
			</main>
		</Box>
	);
};

export default Layout;
