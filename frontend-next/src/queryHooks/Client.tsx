import {
	useMutation,
	type UseMutationOptions,
	type UseMutationResult,
	useQuery,
	useQueryClient,
	type UseQueryResult,
} from "@tanstack/react-query";

import { useApiClient } from "@/context/ApiProvider";

import type { CreateRequest } from "../../apis";
import { type ClientDto } from "../../models";

export const useClients = (): UseQueryResult<ClientDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["clients"],
		queryFn: async () => {
			return await client.clientApi.readAll();
		},
	});
};

export const useClient = (id: number): UseQueryResult<ClientDto> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["client", id],
		queryFn: async () => {
			return await client.clientApi.readOne({ id });
		},
	});
};

export const useClientCount = (): UseQueryResult<number> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["clientCount"],
		queryFn: async () => {
			return await client.clientApi.count();
		},
	});
};

interface IUseMutateClient {
	options?: Omit<UseMutationOptions<ClientDto, Error, CreateRequest>, "mutationFn">;
}

export const useMutateClient = ({
	options,
}: IUseMutateClient): UseMutationResult<ClientDto, Error, CreateRequest> => {
	const client = useApiClient();
	const queryClient = useQueryClient();

	return useMutation({
		mutationFn: async (data) => await client.clientApi.create(data),
		onSettled: (...all) => {
			void queryClient.invalidateQueries({ queryKey: ["clientCount", "clients"] });
			if (options?.onSettled != null) {
				options.onSettled(...all);
			}
		},
		...options,
	});
};
