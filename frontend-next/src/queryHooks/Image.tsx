import { useQuery, type UseQueryResult } from "@tanstack/react-query";

interface IUseImage {
	key?: string | number;
	width?: number;
	height?: number;
}

export const useImage = ({
	key,
	width = 200,
	height = 300,
}: IUseImage): UseQueryResult<{ url: string }> => {
	return useQuery({
		queryKey: ["image", key],
		queryFn: async () => {
			return await fetch(`https://picsum.photos/${width}/${height}`).then(async (response) => {
				const resp = await response.blob();
				return { url: URL.createObjectURL(resp) };
			});
		},
	});
};
