import {
	useMutation,
	type UseMutationOptions,
	type UseMutationResult,
	useQuery,
	useQueryClient,
	type UseQueryResult,
} from "@tanstack/react-query";

import { useApiClient } from "@/context/ApiProvider";

import { type Create2Request } from "../../apis";
import { type BookDto } from "../../models";

export const useBooks = (): UseQueryResult<BookDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["books"],
		queryFn: async () => {
			return await client.bookApi.readAll2();
		},
	});
};

export const useBookById = (id: number): UseQueryResult<BookDto> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["book", id],
		queryFn: async () => {
			return await client.bookApi.readOne2({ id });
		},
	});
};

export const useFreeBooks = (): UseQueryResult<BookDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["freeBooks"],
		queryFn: async () => {
			return await client.bookApi.readAllFree();
		},
	});
};

export const useBorrowedBooks = (): UseQueryResult<BookDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["borrowedBooks"],
		queryFn: async () => {
			return await client.borrowingApi
				.notReturned()
				.then((borrowings) => borrowings.flatMap((borrowing) => borrowing.borrowed));
		},
	});
};

export const useBookCount = (): UseQueryResult<number> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["bookCount"],
		queryFn: async () => {
			return await client.bookApi.count2();
		},
	});
};

interface IUseMutateBook {
	options?: Omit<UseMutationOptions<BookDto, Error, Create2Request>, "mutationFn">;
}

export const useMutateBook = ({
	options,
}: IUseMutateBook): UseMutationResult<BookDto, Error, Create2Request> => {
	const client = useApiClient();
	const queryClient = useQueryClient();

	return useMutation({
		mutationFn: async (data) => await client.bookApi.create2(data),
		onSettled: (...all) => {
			void queryClient.invalidateQueries({ queryKey: ["bookCount", "books", "freeBooks"] });
			if (options?.onSettled != null) {
				options.onSettled(...all);
			}
		},
		...options,
	});
};
