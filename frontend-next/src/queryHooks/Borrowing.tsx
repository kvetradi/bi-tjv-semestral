import {
	useMutation,
	type UseMutationOptions,
	type UseMutationResult,
	useQuery,
	useQueryClient,
	type UseQueryResult,
} from "@tanstack/react-query";

import { useApiClient } from "@/context/ApiProvider";

import {
	type Create1Request,
	type FindBorrowingsForBookRequest,
	type Update1Request,
} from "../../apis";
import { type BorrowingDto } from "../../models";

export const useBorrowings = (): UseQueryResult<BorrowingDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["borrowings"],
		queryFn: async () => {
			return await client.borrowingApi.readAll1();
		},
	});
};

export const useNotReturnedBorrowings = (): UseQueryResult<BorrowingDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["unreturnedBorrowings"],
		queryFn: async () => {
			return await client.borrowingApi.notReturned();
		},
	});
};

export const useBorrowing = (id: number): UseQueryResult<BorrowingDto> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["borrowing", id],
		queryFn: async () => {
			return await client.borrowingApi.readOne1({ id });
		},
	});
};

export const useBorrowingCount = (): UseQueryResult<number> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["borrowingCount"],
		queryFn: async () => {
			return await client.borrowingApi.count1();
		},
	});
};

export const useBorrowingsForBookById = ({
	id,
}: FindBorrowingsForBookRequest): UseQueryResult<BorrowingDto[]> => {
	const client = useApiClient();

	return useQuery({
		queryKey: ["bookBorrowings", id],
		queryFn: async () => {
			return await client.borrowingApi.findBorrowingsForBook({ id });
		},
	});
};

interface IUseCreateMutateBorrowing {
	options?: Omit<UseMutationOptions<BorrowingDto, any, Create1Request>, "mutationFn">;
}

export const useCreateMutateBorrowing = ({
	options,
}: IUseCreateMutateBorrowing): UseMutationResult<BorrowingDto, any, Create1Request> => {
	const client = useApiClient();
	const queryClient = useQueryClient();

	return useMutation({
		mutationFn: async (data) => await client.borrowingApi.create1(data),
		onSettled: (...all) => {
			void queryClient.invalidateQueries({
				queryKey: ["borrowings", "borrowingCount", "unreturnedBorrowings"],
			});
			if (options?.onSettled != null) {
				options.onSettled(...all);
			}
		},
		...options,
	});
};

interface IUseMutateBorrowing {
	options?: Omit<UseMutationOptions<BorrowingDto, any, Update1Request>, "mutationFn">;
}

export const useMutateBorrowing = ({
	options,
}: IUseMutateBorrowing): UseMutationResult<BorrowingDto, any, Update1Request> => {
	const client = useApiClient();
	const queryClient = useQueryClient();

	return useMutation({
		mutationFn: async (data) => {
			// eslint-disable-next-line
			return await client.borrowingApi.update1(data);
		},
		onSettled: (...all) => {
			void queryClient.invalidateQueries({
				queryKey: ["unreturnedBorrowings", "borrowing", all[0]?.id],
			});
			if (options?.onSettled != null) {
				options.onSettled(...all);
			}
		},
		...options,
	});
};
