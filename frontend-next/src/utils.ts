export const formatDate = (
	date: Date | null | undefined,
	defaultReturn: string = "---",
): string => {
	if (date == null) return defaultReturn;

	return Intl.DateTimeFormat().format(date);
};
