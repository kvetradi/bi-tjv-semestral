import { createTheme } from "@mui/material";

import { grey } from "@/styles/designColors";

import sharedTheme from "./theme";

const theme = createTheme({
	palette: {
		mode: "dark",
	},
	...sharedTheme,
	components: {
		MuiLink: {
			styleOverrides: {
				root: {
					color: grey["000"],
					textDecoration: "none",
				},
			},
		},
		MuiMenu: {
			styleOverrides: {
				root: {
					color: grey["000"],
				},
			},
		},
		MuiButton: {
			styleOverrides: {
				root: {
					color: grey["000"],
				},
			},
		},
		...sharedTheme.components,
	},
});

export default theme;
