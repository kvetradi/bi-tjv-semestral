import { createTheme } from "@mui/material";

import { blue, green, grey, orange, red } from "./designColors";
import sharedTheme from "./theme";

const theme = createTheme({
	palette: {
		mode: "light",
		primary: {
			light: blue["300"],
			main: blue["500"],
			dark: blue["700"],
		},
		secondary: {
			light: orange["100"],
			main: orange["300"],
			dark: orange["500"],
		},
		info: {
			light: grey["000"],
			main: grey["400"],
		},
		success: {
			light: green["000"],
			main: green["500"],
		},
		error: {
			light: red["000"],
			main: red["700"],
		},
		warning: {
			light: orange["000"],
			main: orange["300"],
		},
	},
	...sharedTheme,
	components: {
		...sharedTheme.components,
		MuiLink: {
			styleOverrides: {
				root: {
					color: grey["800"],
					textDecoration: "none",
				},
			},
		},
		MuiMenu: {
			styleOverrides: {
				root: {
					color: grey["800"],
				},
			},
		},
		MuiButton: {
			styleOverrides: {
				root: {
					color: grey["800"],
				},
			},
		},
		MuiCssBaseline: {
			styleOverrides: {
				body: {
					backgroundColor: "#f2f2f2",
				},
			},
		},
	},
});

export default theme;
