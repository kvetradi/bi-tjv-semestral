import { Button, type ButtonProps, CircularProgress } from "@mui/material";

interface ILoadingButton extends ButtonProps {
	isLoading?: boolean;
}

export const LoadingButton = ({ isLoading, ...rest }: ILoadingButton) => {
	if (isLoading === true) {
		return (
			<Button {...rest} disabled={true}>
				<CircularProgress />
			</Button>
		);
	}

	return <Button {...rest} />;
};
