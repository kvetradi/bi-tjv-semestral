import { Button, type ButtonProps } from "@mui/material";

import Link, { type ILink } from "@/components/Link";

export interface ILinkButton extends Omit<ButtonProps, "children"> {
	linkProps: ILink;
	text?: string;
}

export const LinkButton = ({ linkProps, text, ...rest }: ILinkButton) => {
	return (
		<Button {...rest}>
			<Link {...linkProps}>{text}</Link>
		</Button>
	);
};
