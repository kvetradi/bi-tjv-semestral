import { Typography, type TypographyProps } from "@mui/material";

export const HeadingXS = ({ ...rest }: Omit<TypographyProps, "variant">) => {
	return <Typography variant="h6" {...rest} />;
};

export const HeadingS = ({ ...rest }: Omit<TypographyProps, "variant">) => {
	return <Typography variant="h5" {...rest} />;
};

export const Heading = ({ ...rest }: Omit<TypographyProps, "variant">) => {
	return <Typography variant="h4" {...rest} />;
};

export const HeadingL = ({ ...rest }: Omit<TypographyProps, "variant">) => {
	return <Typography variant="h3" {...rest} />;
};

export const HeadingXL = ({ ...rest }: Omit<TypographyProps, "variant">) => {
	return <Typography variant="h2" {...rest} />;
};
