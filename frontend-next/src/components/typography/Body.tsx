import { Typography, type TypographyProps } from "@mui/material";
import { type JSX } from "react";

export const BodyS = ({ ...rest }: Omit<TypographyProps, "variant">): JSX.Element => {
	return <Typography variant="body2" {...rest} />;
};

export const BodyM = ({ ...rest }: Omit<TypographyProps, "variant">): JSX.Element => {
	return <Typography variant="body1" {...rest} />;
};
