import { Paper, Stack } from "@mui/material";

import { Heading } from "@/components/typography/Heading";
import { useBookCount } from "@/queryHooks/Book";
import { useBorrowingCount } from "@/queryHooks/Borrowing";
import { useClientCount } from "@/queryHooks/Client";

export const Statistics = () => {
	const { data: bookCount } = useBookCount();
	const { data: clientCount } = useClientCount();
	const { data: borrowingCount } = useBorrowingCount();

	return (
		<Paper sx={{ p: 4 }}>
			<Stack>
				<Heading>Statistics</Heading>
				<Stack>Books: {bookCount ?? "-"}</Stack>
				<Stack>Clients: {clientCount ?? "-"}</Stack>
				<Stack>Borrowings: {borrowingCount ?? "-"}</Stack>
			</Stack>
		</Paper>
	);
};
