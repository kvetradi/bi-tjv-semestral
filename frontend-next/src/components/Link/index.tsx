import { Link as MUILink, type LinkProps } from "@mui/material";
import NextLink from "next/link";
import React, { forwardRef } from "react";

const LinkBehavior = forwardRef(function InternalLink(props, ref) {
	// @ts-expect-error this is ok for now
	return <NextLink ref={ref} {...props} />;
});

export interface ILink extends LinkProps {}

const Link = ({ ...rest }: ILink): JSX.Element => <MUILink {...rest} component={LinkBehavior} />;

export default Link;
