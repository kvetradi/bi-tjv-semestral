import { Card, CardActions, CardContent, CardMedia, Skeleton } from "@mui/material";

import { LinkButton } from "@/components/button/LinkButton";
import { HeadingS, HeadingXS } from "@/components/typography/Heading";
import { useImage } from "@/queryHooks/Image";

import { type BookDto } from "../../../models";

interface IBookCard {
	book: BookDto;
}

export const BookCard = ({ book }: IBookCard) => {
	const { data } = useImage({ key: book.id, width: 340, height: 120 });

	return (
		<Card sx={{ width: 340 }}>
			{data?.url !== undefined ? (
				<CardMedia sx={{ height: 120 }} image={data?.url ?? ""} title="book cover" />
			) : (
				<Skeleton height={120} sx={{ width: "100%" }} />
			)}
			<CardContent>
				<HeadingS>{book.name}</HeadingS>
				<HeadingXS sx={{ color: "grey.500" }}>{book.authorName}</HeadingXS>
			</CardContent>
			<CardActions>
				<LinkButton text="Learn more" linkProps={{ href: `/book/${book.id}` }} />
			</CardActions>
		</Card>
	);
};
