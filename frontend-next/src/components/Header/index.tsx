import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import CloudCircleIcon from "@mui/icons-material/CloudCircle";
import { AppBar, Box, Container, Toolbar } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";
import React from "react";

import Link from "@/components/Link";
import Navbar from "@/components/Navbar";
import { useColorMode } from "@/context/ColorMode";

const Header = (): JSX.Element => {
	const theme = useTheme();
	const { toggleColorMode } = useColorMode();

	return (
		<Box>
			<AppBar position="static">
				<Container>
					<Toolbar>
						<IconButton size="large" edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
							<Link href="/">
								<CloudCircleIcon color="secondary" fontSize="large" />
							</Link>
						</IconButton>
						<Navbar />
						{theme.palette.mode} mode
						<IconButton sx={{ ml: 1 }} onClick={toggleColorMode} color="inherit">
							{theme.palette.mode === "dark" ? <Brightness7Icon /> : <Brightness4Icon />}
						</IconButton>
					</Toolbar>
				</Container>
			</AppBar>
		</Box>
	);
};

export default Header;
