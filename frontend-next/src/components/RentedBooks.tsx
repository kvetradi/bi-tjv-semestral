import { Stack, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";

import { Heading } from "@/components/typography/Heading";
import { useBorrowedBooks } from "@/queryHooks/Book";

export const RentedBooks = () => {
	const { data, isSuccess } = useBorrowedBooks();

	return (
		<Stack>
			<Heading>Rented books</Heading>
			<Table>
				<TableHead></TableHead>
				<TableBody>
					{isSuccess &&
						data?.map((book) => (
							<TableRow key={book.id}>
								<TableCell>{book.id}</TableCell>
								<TableCell>{book.name}</TableCell>
								<TableCell>{book.authorName}</TableCell>
								<TableCell>{book.releaseDate?.toLocaleDateString() ?? ""}</TableCell>
							</TableRow>
						))}
				</TableBody>
			</Table>
		</Stack>
	);
};
