import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";
import { TableCell, TableRow } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import { useState } from "react";

import { TableRowDetail } from "@/components/table/borrowing/row/components/TableRowDetail";
import { formatDate } from "@/utils";

import { type BorrowingDto } from "../../../../../models";

interface IBorrowingTableRow {
	borrowing: BorrowingDto;
}

export const BorrowingTableRow = ({ borrowing }: IBorrowingTableRow) => {
	const [open, setOpen] = useState(false);

	return (
		<>
			<TableRow hover key={borrowing.id}>
				<TableCell>
					<IconButton
						onClick={() => {
							setOpen((prev) => !prev);
						}}
					>
						{open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
					</IconButton>
				</TableCell>
				<TableCell>{`${borrowing.client?.forename} ${borrowing.client?.surname}`}</TableCell>
				<TableCell>{formatDate(borrowing.borrowDate)}</TableCell>
				<TableCell>{formatDate(borrowing.returnDate)}</TableCell>
				<TableCell>{borrowing.borrowed?.length}</TableCell>
				<TableCell>{borrowing.fee}</TableCell>
				<TableCell>{borrowing.note}</TableCell>
			</TableRow>
			<TableRow>
				<TableRowDetail borrowing={borrowing} isOpen={open} />
			</TableRow>
		</>
	);
};
