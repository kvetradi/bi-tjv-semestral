import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";
import UndoIcon from "@mui/icons-material/Undo";
import { TableCell, TableRow } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import { useState } from "react";

import { TableRowDetail } from "@/components/table/borrowing/row/components/TableRowDetail";
import { formatDate } from "@/utils";

import { type BorrowingDto } from "../../../../../models";

interface INotReturnedRow {
	borrowing: BorrowingDto;
	onReturn: (borrowing: BorrowingDto) => void;
}

export const NotReturnedRow = ({ borrowing, onReturn }: INotReturnedRow) => {
	const [open, setOpen] = useState(false);

	return (
		<>
			<TableRow hover key={borrowing.id}>
				<TableCell>
					<IconButton
						onClick={() => {
							setOpen((prev) => !prev);
						}}
					>
						{open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
					</IconButton>
				</TableCell>
				<TableCell>{`${borrowing.client?.forename} ${borrowing.client?.surname}`}</TableCell>
				<TableCell>{formatDate(borrowing.borrowDate)}</TableCell>
				<TableCell>{borrowing.borrowed?.length}</TableCell>
				<TableCell>
					<IconButton
						onClick={() => {
							onReturn(borrowing);
						}}
					>
						<UndoIcon />
					</IconButton>
				</TableCell>
			</TableRow>
			<TableRow>
				<TableRowDetail borrowing={borrowing} isOpen={open} />
			</TableRow>
		</>
	);
};
