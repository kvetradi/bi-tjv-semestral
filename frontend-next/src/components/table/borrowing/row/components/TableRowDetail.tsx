import { Collapse, Stack, TableCell } from "@mui/material";
import Box from "@mui/material/Box";

import { BodyM, BodyS } from "@/components/typography/Body";
import { HeadingXS } from "@/components/typography/Heading";

import { type BorrowingDto } from "../../../../../../models";

interface ITableRowDetail {
	borrowing: BorrowingDto;
	isOpen: boolean;
}

export const TableRowDetail = ({ borrowing, isOpen }: ITableRowDetail) => {
	return (
		<TableCell
			sx={{ paddingTop: 0, paddingBottom: 0, border: !isOpen ? "none" : undefined }}
			colSpan={8}
		>
			<Collapse in={isOpen} unmountOnExit timeout="auto">
				<Box sx={{ py: 2, px: 4 }}>
					<Stack>
						<HeadingXS>Details</HeadingXS>
						<Stack direction={{ xs: "column", md: "row" }} gap={5}>
							<Stack>
								<BodyM>Books Borrowed</BodyM>
								{borrowing.borrowed?.map((book) => (
									<Stack direction="row" gap={4} alignItems="center" key={`${borrowing.id}-${book.id}`}>
										<BodyM>{book.name}</BodyM>
										<BodyS>{book.authorName}</BodyS>
									</Stack>
								))}
							</Stack>
							<Stack>
								<BodyM>Borrowed by</BodyM>
								<BodyM>{`${borrowing.client?.forename} ${borrowing.client?.surname}`}</BodyM>
							</Stack>
						</Stack>
					</Stack>
				</Box>
			</Collapse>
		</TableCell>
	);
};
