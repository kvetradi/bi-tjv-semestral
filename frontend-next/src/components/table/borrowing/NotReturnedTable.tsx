import {
	CircularProgress,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
} from "@mui/material";
import { toast } from "react-toastify";

import { NotReturnedRow } from "@/components/table/borrowing/row/NotReturnedRow";
import { BodyM } from "@/components/typography/Body";
import { useMutateBorrowing, useNotReturnedBorrowings } from "@/queryHooks/Borrowing";

import type { BorrowingDto } from "../../../../models";

export const NotReturnedTable = () => {
	const { data, isLoading, isSuccess, refetch } = useNotReturnedBorrowings();
	const { mutateAsync } = useMutateBorrowing({
		options: {
			onSuccess: () => {
				toast.success("Borrowing returned");
				void refetch();
			},
			onError: () => {
				toast.error("Something went wrong");
			},
		},
	});

	const returnBorrowing = (borrowing: BorrowingDto) => {
		void mutateAsync({
			borrowingDto: { ...borrowing, returnDate: new Date(Date.now()) },
			id: borrowing.id as number,
		});
	};

	if (isLoading) return <CircularProgress />;

	if (!isSuccess) return <BodyM>Error loading data from server</BodyM>;

	return (
		<TableContainer component={Paper}>
			<Table>
				<TableHead>
					<TableRow>
						<TableCell>Detail</TableCell>
						<TableCell>Borrowed by</TableCell>
						<TableCell>Borrow date</TableCell>
						<TableCell>#books borrowed</TableCell>
						<TableCell>Return</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{data.map((borrowing) => (
						<NotReturnedRow borrowing={borrowing} key={borrowing.id} onReturn={returnBorrowing} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};
