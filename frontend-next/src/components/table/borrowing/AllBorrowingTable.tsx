import {
	CircularProgress,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
} from "@mui/material";

import { BorrowingTableRow } from "@/components/table/borrowing/row/BorrowingTableRow";
import { BodyM } from "@/components/typography/Body";
import { useBorrowings } from "@/queryHooks/Borrowing";

export const AllBorrowingTable = () => {
	const { data, isLoading, isSuccess } = useBorrowings();

	if (isLoading) return <CircularProgress />;

	if (!isSuccess) return <BodyM>Error loading data from server</BodyM>;

	return (
		<TableContainer component={Paper}>
			<Table>
				<TableHead>
					<TableRow>
						<TableCell />
						<TableCell>Borrowed by</TableCell>
						<TableCell>Borrow date</TableCell>
						<TableCell>Return date</TableCell>
						<TableCell>#books borrowed</TableCell>
						<TableCell>Fee</TableCell>
						<TableCell>Note</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{data.map((borrowing) => (
						<BorrowingTableRow borrowing={borrowing} key={borrowing.id} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};
