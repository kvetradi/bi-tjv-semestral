import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Menu, MenuItem as MuiMenuItem, type MenuItemProps, MenuList } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import React, { useState } from "react";

import MenuItem, { type MenuItemType } from "@/components/Navbar/MenuItem";

interface IMultiMenuItem extends MenuItemProps {
	item: MenuItemType;
}

const MultiMenuItem = ({ item, sx, ...rest }: IMultiMenuItem): JSX.Element => {
	const theme = useTheme();
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	const open = Boolean(anchorEl);

	const close = (): void => {
		setAnchorEl(null);
	};

	const handleOpen = (e: React.MouseEvent<HTMLElement>): void => {
		setAnchorEl((prev) => (prev == null ? e.currentTarget : null));
	};

	return (
		<MuiMenuItem onClick={handleOpen} {...rest}>
			{item.label}
			{!open && <ExpandMoreIcon />}
			{open && <ExpandLessIcon />}
			<Menu
				anchorEl={anchorEl}
				open={open}
				onClose={close}
				sx={{
					"& .MuiPaper-root": {
						background: theme.palette.primary.main,
					},
				}}
			>
				<MenuList
					disablePadding
					sx={{
						minWidth: `${anchorEl?.offsetWidth ?? 0}px`,
						root: { backgroundColor: theme.palette.primary.main },
					}}
				>
					{item.children?.map((child) => (
						<MenuItem
							item={child}
							key={`${item.label}-${item.url}-${child.label}-${child.url}`}
							sx={{ width: "100%" }}
						/>
					))}
				</MenuList>
			</Menu>
		</MuiMenuItem>
	);
};

export default MultiMenuItem;
