import { MenuItem as MuiMenuItem, type MenuItemProps } from "@mui/material";
import React from "react";

import Link from "@/components/Link";
import { type MenuItemType } from "@/components/Navbar/MenuItem";

interface SingleMenuItemType extends Omit<MenuItemType, "children"> {}

interface ISingleMenuItem extends MenuItemProps {
	item: SingleMenuItemType;
}

const SingleMenuItem = ({ item, sx, ...rest }: ISingleMenuItem): JSX.Element => {
	return (
		<MuiMenuItem key={`${item.label}-${item.url}`} sx={sx} {...rest}>
			<Link href={item.url} sx={{ width: "100%", color: "grey.100" }}>
				{item.label}
			</Link>
		</MuiMenuItem>
	);
};

export default SingleMenuItem;
