import { type MenuItemProps } from "@mui/material";

import MultiMenuItem from "@/components/Navbar/MultiMenuItem";
import SingleMenuItem from "@/components/Navbar/SingleMenuItem";

export type MenuItemType = SingleItemType | MultiItemType;

interface SingleItemType {
	label: string;
	url: string;
	children?: never;
}

interface MultiItemType {
	label: string;
	children: MenuItemType[];
	url?: never;
}

interface IMenuItem extends MenuItemProps {
	item: MenuItemType;
}

const MenuItem = ({ item }: IMenuItem): JSX.Element => {
	const hasChildren = (): boolean => {
		return item.children !== undefined && item.children.length > 0;
	};
	return hasChildren() ? <MultiMenuItem item={item} /> : <SingleMenuItem item={item} />;
};

export default MenuItem;
