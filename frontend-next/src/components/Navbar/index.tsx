import { Stack } from "@mui/material";

import MenuItem from "@/components/Navbar/MenuItem";
import { navbarUrls } from "@/components/Navbar/navbarUrls";

const Navbar = (): JSX.Element => {
	return (
		<Stack direction="row" sx={{ flexGrow: 1 }}>
			{navbarUrls.map((item) => (
				<MenuItem item={item} key={`menu-${item.label}-${item.url}`} sx={{ color: "white" }} />
			))}
		</Stack>
	);
};

export default Navbar;
