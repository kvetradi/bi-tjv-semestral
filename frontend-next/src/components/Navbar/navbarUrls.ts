import { type MenuItemType } from "@/components/Navbar/MenuItem";

export const navbarUrls: MenuItemType[] = [
	{
		url: "/",
		label: "Home",
	},
	{
		url: "/books",
		label: "Books",
	},
	{
		label: "Borrowings",
		children: [
			{
				label: "All",
				url: "/borrowing/all",
			},
			{
				label: "Unreturned",
				url: "/borrowing/unreturned",
			},
		],
	},
	{
		label: "New",
		children: [
			{
				label: "Book",
				url: "/book/new",
			},
			{
				label: "Client",
				url: "/client/new",
			},
			{
				label: "Borrowing",
				url: "/borrowing/new",
			},
		],
	},
];
