import { Paper, Skeleton, Stack } from "@mui/material";
import Image from "next/image";

import { Heading, HeadingXS } from "@/components/typography/Heading";
import { useBookById } from "@/queryHooks/Book";
import { useImage } from "@/queryHooks/Image";

interface IBook {
	id: number;
}

export const BookDetail = ({ id }: IBook) => {
	const { data, isLoading, isSuccess } = useBookById(Number(id));
	const { data: imageData } = useImage({ key: data?.id, width: 500, height: 500 });

	if (isLoading) return <Skeleton variant="rounded" />;

	if (!isSuccess) return <Heading>Error loading data from server</Heading>;

	return (
		<Paper sx={{ p: 4 }}>
			<Stack direction="row" gap={4}>
				<Stack>
					{imageData != null ? (
						<Image src={imageData.url} alt="book cover" width={150} height={200} />
					) : (
						<Skeleton variant="rounded" width={150} height={200} />
					)}
				</Stack>
				<Stack flex={1}>
					<Heading>{data?.name}</Heading>
					<HeadingXS>{data?.authorName}</HeadingXS>
					<HeadingXS>{Intl.DateTimeFormat().format(data?.releaseDate)}</HeadingXS>
				</Stack>
			</Stack>
		</Paper>
	);
};
