import { Paper, Skeleton, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import React from "react";

import { Heading } from "@/components/typography/Heading";
import { useBorrowingsForBookById } from "@/queryHooks/Borrowing";

interface IBookRentsList {
	bookId: number;
}

export const BookRentsList = ({ bookId }: IBookRentsList) => {
	const { data } = useBorrowingsForBookById({ id: bookId });

	return (
		<Paper sx={{ p: 4 }}>
			<Heading>History</Heading>
			{data == null ? (
				<Skeleton variant="rounded" width="100%" />
			) : (
				<Table>
					<TableHead>
						<TableRow>
							<TableCell>Borrowed by</TableCell>
							<TableCell>From</TableCell>
							<TableCell>To</TableCell>
							<TableCell>Fee</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{data
							.sort((a, b) => (a.borrowDate?.getTime() ?? 0) - (b.borrowDate?.getTime() ?? 0))
							.map((borrowing) => (
								<TableRow key={borrowing.id}>
									<TableCell>{`${borrowing.client?.forename} ${borrowing.client?.surname}`}</TableCell>
									<TableCell>{Intl.DateTimeFormat().format(borrowing.borrowDate)}</TableCell>
									<TableCell>{Intl.DateTimeFormat().format(borrowing.returnDate)}</TableCell>
									<TableCell>{borrowing.fee}</TableCell>
								</TableRow>
							))}
					</TableBody>
				</Table>
			)}
		</Paper>
	);
};
