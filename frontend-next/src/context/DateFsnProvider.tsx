import {
	LocalizationProvider,
	type LocalizationProviderProps,
	type MuiPickersAdapter,
} from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFnsV3";

export type DateFnsProviderProps<TDate> = Omit<
	LocalizationProviderProps<TDate, any>,
	"dateAdapter"
> & {
	dateAdapter?: new (...args: any) => MuiPickersAdapter<TDate>;
};

export default function DateFnsProvider({ children, ...props }: DateFnsProviderProps<Date>) {
	const { dateAdapter, ...localizationProps } = props;
	return (
		<LocalizationProvider dateAdapter={dateAdapter ?? AdapterDateFns} {...localizationProps}>
			{children}
		</LocalizationProvider>
	);
}
