import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { type ReactNode } from "react";

interface IProps {
	children: ReactNode;
}

export const QueryProvider = ({ children }: IProps) => {
	const client = new QueryClient({
		defaultOptions: {
			queries: {
				refetchOnWindowFocus: false,
				refetchInterval: false,
				refetchOnMount: false,
				refetchIntervalInBackground: false,
			},
		},
	});

	return <QueryClientProvider client={client}>{children}</QueryClientProvider>;
};
