import { createContext, type ReactNode, useContext } from "react";

import { BooksApi, BorrowingsApi, ClientsApi } from "../../apis";
import { Configuration } from "../../runtime";

interface IApiContext {
	bookApi: BooksApi;
	borrowingApi: BorrowingsApi;
	clientApi: ClientsApi;
}

export const ApiClientContext = createContext<IApiContext>({} as unknown as IApiContext);

export function useApiClient(): IApiContext {
	return useContext<IApiContext>(ApiClientContext);
}

interface IProps {
	children: ReactNode;
}

export const ApiProvider = ({ children }: IProps) => {
	const config = new Configuration({});

	return (
		<ApiClientContext.Provider
			value={{
				bookApi: new BooksApi(config),
				borrowingApi: new BorrowingsApi(config),
				clientApi: new ClientsApi(config),
			}}
		>
			{children}
		</ApiClientContext.Provider>
	);
};
